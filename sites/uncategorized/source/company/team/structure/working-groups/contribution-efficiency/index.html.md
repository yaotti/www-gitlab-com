---
layout: markdown_page
title: "Contribution Efficiency"
description: "Implement key business iterations that results in substantial and sustained increases to community contributors & contributions"
canonical_path: "/company/team/structure/working-groups/contribution-efficiency/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property        | Value           |
|-----------------|-----------------|
| Date Created    | October 28rd, 2021 |
| Target End Date | April 28rd, 2022 |
| Slack           | [#wg_contribution-efficiency](https://gitlab.slack.com/archives/C0223D98HHC) (only accessible from within the company) |
| Google Doc      | [Working Group Agenda](https://docs.google.com/document/d/1AOgqaslnq-WI1ICSZ1NzSnALf1Va4D5qAD191icAoSI/edit#) (only accessible from within the company) |
| Task Board      | [Issue board](https://gitlab.com/groups/gitlab-com/-/boards/3014703?label_name%5B%5D=Contribution%20Efficiency) (items above the cut-line) |

## Business Goal

Implement key business iterations that results in substantial and sustained increases to community contributors & contributions

### Exit Criteria (10% completed)

1. Implement 5 experiments aimed at increasing community contributors and document results broadly 
   1. Decreasing community MR Review time
   1. Create additional retention recognition 
   1. Education sector outreach 
   1. Hackathon continuation 
   1. Outreach effort
1. Improve upon contribution process based on successful experiment, optimize into our day-to-day operations 
1. Delivery key joint strategy of increasing community contributors & contributions and hold company-wide AMA
1. Setup first iteration of community cohort team 
1. Create additional mechanisms for community and enterprise team members to participate & contribute
1. Add 2 more awards to community to add on to release MVP
1. Deliver 3 key iterations to our contribution guidelines 
1. Fully implement MR Coach specialties (Development, Test, Docs & etc)
1. Experiment with defining contributor specialties based on MR coaches 
1. Define follow up working process between Community Relations and Contributor Success teams


### Roles and Responsibilities

| Working Group Role    | Person                                               | Title                                                      |
|-----------------------|------------------------------------------------------|------------------------------------------------------------|
| Executive Sponsor     | [Mek Stittri](https://gitlab.com/meks)               | VP of Quality                                              |
| Functional Lead       | [Kyle Wiebers](https://gitlab.com/kwiebers)          | Engineering Manager, Engineering Productivity              |
| Functional Lead       | [John Coghlan](https://gitlab.com/johncoghlan)       | Manager, Community Relations                               |
| Functional Lead       | [Christos Bacharakis](https://gitlab.com/cbacharakis)| Senior Code Contributor Program Manager                    |
| Member                | [Rémy Coutable](https://gitlab.com/rymai)            | Staff Backend Engineer, Engineering Productivity           |
| Member                | [Tanya Pazitny](https://gitlab.com/tpazitny)         | Director of Quality Engineering                            |
| Member                | [Matija Čupić](https://gitlab.com/matteeyah)         | Fullstack Engineer, Contributor Success                    |
| Member                | [Marshall Cottrell](https://gitlab.com/marshall007)  | Principal, Strategy and Operations (Technical)             |           
