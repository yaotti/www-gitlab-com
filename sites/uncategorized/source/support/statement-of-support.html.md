---
layout: markdown_page
title: Statement of Support
description: "What is and is not within the scope of support at GitLab."
---

The GitLab Support Team is here to help. This document defines what we support in terms of our products, services, and applications. Part of providing effective support is defining what is outside of the scope of support.

Scope of support, in the simplest terms, is what we support and what we do not. Ideally, we would support everything. However, without reducing the quality of our support or increasing the price of our products this would be impossible. These "limitations" help us to create a more consistent and efficient support experience.

Please understand that any support that might be offered beyond the scope defined here is done
at the discretion of the [Support Engineer](https://about.gitlab.com/job-families/engineering/support-engineer/) and is provided as a courtesy.

- TOC
{:toc}

## **GitLab Self-Managed**

### Starter (Legacy), Premium, and Ultimate Customers

We will help troubleshoot all components bundled with GitLab Omnibus when used
as a packaged part of a GitLab installation. Any assistance with modifications to GitLab, including new functionality, bug-fixes, issues with [alpha features](#alpha-features) or other code changes should go through the [GitLab project's issue tracker](https://gitlab.com/gitlab-org/gitlab/issues). Moreover, support is not offered for local modifications to GitLab source code.

We understand that GitLab is often used in complex environments in combination with a variety of tools. We'll use **commercially-reasonable efforts** to debug components that work alongside GitLab.

If you obtained an Ultimate license as part of GitLab's [Open Source or Education program](https://about.gitlab.com/blog/2018/06/05/gitlab-ultimate-and-gold-free-for-education-and-open-source/),
Support is **not** included unless purchased separately. Please see the [GitLab OSS License/Subscription Details](https://about.gitlab.com/handbook/marketing/community-relations/opensource-program/#support-add-ons-and-additional-services) for additional details.

#### Version Support

Unless otherwise specified in your support contract, we support the current major version and previous two major versions only. For example, as [`14.x` is the current major version](https://about.gitlab.com/releases/), GitLab installations running versions in the `14.x`, `13.x` and `12.x` series are eligible for support.

GitLab only backports fixes, not features and that too on a limited number of prior releases. For more details please visit the [maintenance policy](https://docs.gitlab.com/ee/policy/maintenance.html).

If you contact support about issues you're experiencing while on an unsupported version, we'll link to this section of this page and invite you to upgrade. If you encounter any issues while upgrading to a supported version, please open a new ticket.

#### Installation & New Features

For assistance with first-time installations and the configuration of new features, we highly recommend using our comprehensive [documentation](https://docs.gitlab.com/):

- First-time installation [step by step guide](https://about.gitlab.com/install/)
- [Updating your GitLab instance](https://docs.gitlab.com/ee/update/)
- [High availability](https://docs.gitlab.com/ee/administration/reference_architectures/index.html)
- [Kubernetes clusters](https://docs.gitlab.com/ee/user/project/clusters/)

Alternatively, you can [reach out to our Professional Services team](https://about.gitlab.com/services/) who provide training and assistance with the design and implementation of new features and installations.

If you're facing challenges after attempting the installation of a new deployment or implementing new features, a support ticket can be opened using our [support portal](/support/#contact-support).

#### Gitaly and NFS

Upon the release of GitLab 15.0 (tentatively May 22nd, 2022) technical and engineering support for using NFS to store Git repository data will be officially at end-of-life regardless of your GitLab version. 

You can read more about Gitaly and our NFS support in the [NFS documentation](https://docs.gitlab.com/ee/administration/nfs.html#gitaly-and-nfs-deprecation)

### Free and Community Edition Users

If you are seeking help with your GitLab Free or Community Edition installation, note that
the GitLab Support Team is unable to directly assist with issues with specific installations
of these versions. Please use the following resources instead:

- [GitLab Documentation](https://docs.gitlab.com): Extensive documentation
regarding the possible configurations of GitLab.
- [GitLab Community Forum](https://forum.gitlab.com/): This is the best place to have
a discussion about your Community Edition configuration and options.
- [Stack Overflow](http://stackoverflow.com/questions/tagged/gitlab): Please search
for similar issues before posting your own, as there's a good chance somebody else
had the same issue as you and has already found a solution.

## **GitLab SaaS**

### Bronze (Legacy), Premium, and Ultimate Customers

GitLab.com has a full team of Site Reliability Engineers and Production Engineers monitoring its status 24/7. This means that often, by the time you notice something is amiss, there's someone already looking into it.

We recommend that all GitLab.com customers follow [@gitlabstatus](https://twitter.com/gitlabstatus) on Twitter and use our [status page](https://status.gitlab.com) to keep informed of any incidents.

If you obtained an Ultimate subscription as part of [GitLab's Open Source or Education programs](https://about.gitlab.com/blog/2018/06/05/gitlab-ultimate-and-gold-free-for-education-and-open-source/),
support is **not** included unless purchased separately. Please see the [GitLab OSS License/Subscription Details](https://about.gitlab.com/handbook/marketing/community-relations/opensource-program/#support-add-ons-and-additional-services) for additional details.

### Free Users

Technical and general support for those using the Free version of GitLab SaaS is “Community First”.
Like many other free SaaS products, users are first directed to find support through community
sources such as the following:

- [GitLab Documentation](https://docs.gitlab.com): Extensive documentation
on anything and everything GitLab.
- [GitLab Community Forum](https://forum.gitlab.com): Get help directly from the community. When able, GitLab employees also participate and assist in answering questions.
- [Stack Overflow](http://stackoverflow.com/questions/tagged/gitlab): Please search
for similar issues before posting your own, as there's a good chance someone else
had the same issue as you and has already found a solution.

However, GitLab Support will assist Free users with the following types of issues:

- Your account or repository is in an unusable state. For example, you're unable to log in to your account or access a repository without receiving an error that you cannot bypass.
- Requests related to an email you received regarding a GitLab SaaS security or production incident.

To receive help for these types of issues please [contact support](/support/#contact-support), and be aware that if a Support Engineer determines that your request is more appropriate for community resources they will direct you to them. Please also note that there are no guaranteed response times associated with support tickets submitted by Free users.

### GitLab SaaS Availability

You should follow [@gitlabstatus](https://twitter.com/GitLabStatus) on Twitter for status
updates on GitLab.com, or check [our status page](https://status.gitlab.com/) to see if there is a known service outage and follow the linked issue for more detailed updates.

## **Out of Scope**

The following sections outline what is within the scope of support and what is not for GitLab Self-Managed customers, GitLab SaaS customers, and both customers and Free users of either.

### GitLab Self-Managed Customers

| Out of Scope       | Example        | What's in-scope then?   |
|--------------------|----------------|-------------------------|
| Debugging EFS problems | *GitLab is slow in my HA setup. I'm using EFS.* | EFS and GlusterFS are **not** recommended for HA setups (see our [HA on AWS doc](https://docs.gitlab.com/ee/install/aws/index.html)). GitLab Support can help verify that your HA setup is working as intended, but will not be able to investigate EFS or GlusterFS backend storage issues. |
| Debugging git repository issues stored on NFS (from GitLab 14.0) | *Commits vanished from our `main` branch. We're running 3 Gitaly servers, sharing data using NFS.* | NFS related-issues with Gitaly are supported up to GitLab 13.12, but will not be supported from GitLab 14.0. [Read more about deprecation of Gitaly support for NFS](#gitaly-and-nfs). |
| Troubleshooting non-GitLab Omnibus components | *I'm trying to get GitLab to work with Apache, can you provide some pointers?* | GitLab Support will only assist with the specific components and versions that ship with the GitLab Omnibus package, and only when used as a part of a GitLab installation. |
| Local modifications to GitLab | *We added a button to ring a bell in our office any time an MR was accepted, but now users can't log in.* | GitLab Support would direct you to create a feature request or submit a merge request for code review to incorporate your changes into the GitLab core. |
| Old versions of GitLab | *I'm running GitLab 7.0 and X is broken.* | GitLab Support will invite you to upgrade your installation to a more current release. Only the current and two previous major versions are supported. |
| Instance migration configuration and troubleshooting | *We migrated GitLab to a new instance and cannot SSH into the server.* | GitLab Support will assist with issues that arise from the GitLab components. GitLab Support will not be able to assist with any issues stemming from the server or it's configuration (see GitLab Instance Migration on the [Support page](../support/index.html)). |
| Debugging custom scripts and automations | *We use custom scripts to automate changes to our GitLab deployment, and it is causing problems or downtime.* | GitLab Support will assist in troubleshooting and resolving issues that occur in the course of interacting with an existing GitLab installation. GitLab Support will not be able to assist with debugging or fixing customer-written code used to deploy, upgrade or modify an in-place installation. |
| Installation of GitLab using unofficial, community-contributed methods | *We ran into an error installing GitLab using the FreeBSD package. Please help!* | GitLab Support can only provide support for installation problems encountered when using an [official installation method](../install/index.html). |
| Live Upgrade Assistance for GitLab installed using unofficial, community-contributed methods | *We installed GitLab using the Arch Linux community package and would like to request live upgrade assistance* | GitLab Support can only provide Live Upgrade Assistance when GitLab is installed using an [official installation method](../install/index.html). |
| Step-by-step instructions for upgrading GitLab on self-hosted infrastructure | *We installed one of the HA reference architectures and need to know how and in which order to update the specific nodes* | GitLab documentation describes the parts of a system that need to be upgraded, and describes certain dependencies. Because so many configurations are possible, we do not attempt to document all of them. We do provide support for the GitLab components of your setup, and can provide [Live Upgrade Assistance](https://about.gitlab.com/support/scheduling-live-upgrade-assistance.html), including a review of your upgrade plan. |
| Running raw SQL queries that modify the GitLab database | *We used an mass update query in the GitLab database to change all of our usernames, and we are now experiencing issues as a result of that change.* | GitLab Support will direct you to rollback to a known working backup, and then assist with debugging and resolving the problem via a safer method. |

### GitLab SaaS Customers

| Out of Scope       | Example        | What's in-scope then?   |
|--------------------|----------------|-------------------------|
| Troubleshooting non-GitLab components | *How do I merge a branch?* | .com Support will happily answer any questions and help troubleshoot any of the components of GitLab |
| Consulting on language or environment-specific configuration | *I want to set up a YAML linter CI task for my project. How do I do that?* | The Support Team will help you find the GitLab documentation for the related feature and can point out common pitfalls when using it. |

### All Self-Managed & SaaS Users

#### Training

GitLab Support is unable to provide training on the use of the underlying technologies that GitLab relies upon. GitLab is a product aimed at technical users, and we expect our users and customers to be versed in the basic usage of the technologies related to features that they're seeking support for.

For example, a customer looking for help with a Kubernetes integration should understand Kubernetes to the extent that they could retrieve log files or perform other basic tasks without in-depth instruction.

#### Git and LFS

We're unable to assist in troubleshooting issues with specific Git commands and cannot provide training for using Git. If the latter is needed, [official Git documentation](https://git-scm.com/docs) is comprehensive.
The same applies to [Git Large File Storage](https://git-lfs.github.com/).
For both topics, GitLab-relevant aspects are collected in
[our summary documentation](https://docs.gitlab.com/ee/topics/git/). 

#### CI/CD

GitLab Support cannot assist with debugging specific commands or scripts included in your `.gitlab-ci.yml` file. We also cannot troubleshoot issues outside of the configuration or setup of private GitLab Runner host machines.

GitLab provides a range of [CI/CD templates](https://gitlab.com/gitlab-org/gitlab-foss/tree/master/lib/gitlab/ci/templates) that can be included in your pipeline. Some of these templates enable features like  [Secret Detection](https://docs.gitlab.com/ee/user/application_security/secret_detection/), [Code Quality](https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html) analysis and [Static Application Security Testing](https://docs.gitlab.com/ee/user/application_security/sast/) while other templates are [examples](https://docs.gitlab.com/ee/ci/examples/) designed to demonstrate functionality like [Load Performance Testing](https://docs.gitlab.com/ee/user/project/merge_requests/load_performance_testing.html) or [Accessibility testing](https://docs.gitlab.com/ee/user/project/merge_requests/accessibility_testing.html).  GitLab Support will assist with documented customizations of these templates. Assistance with direct modifications to these templates or non-documented customizations is out of scope for GitLab Support. We welcome merge requests to improve the functionality of these templates.

For example:

*   **In Scope**: I want to [customize the settings](https://docs.gitlab.com/ee/user/application_security/secret_detection/#custom-settings-example) so that `SECRET_DETECTION_HISTORIC_SCAN` is `true`.
*   **Out of Scope**: I copied `Security/License-Scanning.gitlab-ci.yml` into my repository and am trying to modify it. 

#### Third Party Applications & Integrations

GitLab Support cannot assist with the configuration or troubleshooting of third party applications, integrations, and services that may interact with GitLab. We can ensure that GitLab itself is sending properly formatted data to a third party in the bare-minimum configuration.

For example:

- _"I can't get Jenkins to run builds kicked off by GitLab. Please help me figure out what is going on with my Jenkins server."_

#### Configuration of Deployment Environments

GitLab Support cannot assist with infrastructure, in terms of the creation, configuration, or maintenance of on-premises, cloud, or infrastructure-as-code environments beyond the general guidance from what is provided in our documentation. This includes, but is not limited to, the configuration of firewalls, private networks, virtual machines, and secrets.

#### Creation of SSL/TLS Certificates

GitLab Support cannot assist with the creation of SSL/TLS certificates, certificate signing requests, or the creation of certificate authorities.

## Alpha & Beta Features

<!-- Any changes made to this section should be reflected in https://docs.gitlab.com/ee/policy/alpha-beta-support.html and https://about.gitlab.com/handbook/product/gitlab-the-product/#alpha-beta-ga -->

### Alpha Features

{:.no_toc}

[Alpha](https://about.gitlab.com/handbook/product/gitlab-the-product/#alpha-beta-ga)
features are not yet completely tested for quality and stability, may contain bugs or errors, and prone to see breaking changes in the future. As such, support is not provided for these features and issues with them or other code changes should be opened in the [GitLab issue tracker](https://gitlab.com/gitlab-org/gitlab/issues).

### Beta Features

{:.no_toc}

Your Support Contract will cover support for [Beta features](https://about.gitlab.com/handbook/product/gitlab-the-product/#closed-beta). However, because they are not yet completely tested for quality and stability, may contain bugs or errors, and may be prone to see breaking changes in the future, troubleshooting will require more time, usually need assistance from Development, that support will be conducted on a **commercially-reasonable effort** basis.
