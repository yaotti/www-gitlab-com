---
title: "GitLab's Functional Group Updates: September 4th - September 15th" # replace "MMM" with the current month, and "DD-DD" with the date range
author: Chloe Whitestone
author_gitlab: chloemw
author_twitter: drachanya
categories: company
image_title: '/images/blogimages/functional-group-update-blog-cover.jpg'
description: "The Functional Groups at GitLab give an update on what they've been working on."
tags: inside GitLab, functional group updates
---

<!-- beginning of the intro - leave it as is -->

Every day from Monday to Thursday, right before our [GitLab Team call](/handbook/#team-call), a different Functional Group gives an [update](/handbook/group-conversations/) to our team.

The format of these calls is simple and short where they can either give a presentation or quickly walk the team through their agenda.

<!-- more -->

<!-- end of the intro -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Backend Team

[Presentation slides](https://docs.google.com/presentation/d/1C484YbbT6R29oSR9fzkH9v2-h5uE5w0M0BElBnFAL78/edit)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/WPZZH0BxYWo" frameborder="0" allowfullscreen="true"></iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### CI/CD Team

[Presentation slides](https://docs.google.com/presentation/d/1TEfBqlop2h2OOAZMrt5gkzt6quL6x_OsQtza0FyBWLw/edit)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/c7XPbVQQx0I" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Build Team

[Presentation slides](https://docs.google.com/a/gitlab.com/presentation/d/1ocFPjAGsCuY-BJW-X1z79G043tIAaxHaBf7E2A3466k/present)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/iOkeJBkUbBA" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Support Team

[Presentation slides](https://docs.google.com/presentation/d/1EizMPiTJFYm7R7Av6J7DguR_Crgo_t8pufLYKoGC5sU/)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/aD4LsqgB7mE" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Infrastructure Team

[Presentation slides](https://docs.google.com/a/gitlab.com/presentation/d/1HuQNwe__Bc2L4rqHXOTyWvf3nhdgAU3fT12gQdJstiQ/edit?usp=sharing)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/pnuphOmnotY" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

Questions? Leave a comment below or tweet [@GitLab](https://twitter.com/gitlab)! Would you like to join us? Check out our [job openings](/jobs/)!
