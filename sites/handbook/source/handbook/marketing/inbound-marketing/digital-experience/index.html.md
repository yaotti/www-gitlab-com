---
layout: handbook-page-toc
title: Digital Experience Handbook
description: >-
  Learn more about the Digital Experience purpose, vision, mission, objective
  and more in this handbook.
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Overview

### Purpose

*Why we exist*

We take a customer centric approach to educating Buyers on how GitLab’s DevOps Platform solves their problems.


### Vision

*Where we’re going*

A future where **everyone can contribute**, consumers become contributors and we greatly increase the rate of human progress.

### Mission

*What we do*

We drive continual improvement to GitLab’s user journeys and conversion funnel.

### Objectives

1. Improve Buyer Experience of the Marketing Site
2. Education Buyers on the benefits of GitLab
3. Increase Conversion Rates

### Strategy

1. **Experimentation:** Good ideas come from everywhere
    - Anyone can propose A/B tests
    - Proven A/B test winners go live
    - Results documented to reduce re-testing known saves
2. **Listening:** Voice of Buyer program including:
    - Add feedback tab to core parts of Marketing site
    - Monthly reporting on signals and trends
3. **Personalization:** Right content/right segment/right time
    - Increase access to data sources
    - Persistent data layer to build a one to many relationship
4. **Buyer Experience:** Shift to a buyer obsessed, customer centric organization
    - Understand our buyer’s journey & optimize for each segment
    - Optimize path to purchase funnel for each segment

### Principles

1. **Understand** We need to understand the challenge before we can solve it.
    1. We strive to **understand** the needs of prospective customers.
2. **Trust** We trust our process, we trust each other.
    1. We **respect** each other.
    2. We're **transparent** with each other.
3. **Inclusive** we create psychological safety by celebrating our differences and using them as an asset.
    1. **Empathy** we creating things for our prospective customers.
    2. **Supportive** We support each other and other teams to deliver the best results.
        1. We provide feedback to support each other.
    3. **Accessible** we care about delivering experiences for everyone.
4. **Intentional** simple over complicated.
    1. We meet prospective customers where they’re at.
    2. We strive to deliver the right message at the right time to the right person.
5. **Opinion** We have strong opinions that we hold loosely.
6. **Results** We focus on results by being:
    1. **Actionable**
    2. **Measurable**
7. **Iteration** When applying the [MVC](/handbook/product/product-principles/#the-minimal-viable-change-mvc) approach, we make things smaller by reducing the scope of the jobs-to-be-done rather than sacrificing the end goal.
    1. **Experimentation** We’re driven by experimentation and judge success with data.
    2. **Incremental** We take large tasks and break them down into multiple iterative deliverables.

## Team

| Team Member | Role | Contact Info |
| ----------- | ---- | ------------ |
| [**Michael Preuss**](/company/team/#mpreuss22) | Director, Digital Experience | - Email: [mpreuss@gitlab.com](mailto:mpreuss@gitlab.com)<br>- GitLab: [@mpreuss22](https://gitlab.com/mpreuss22)<br>- Slack: @mpreuss22<br>- ReadMe: [michael-preuss](/handbook/marketing/readmes/michael-preuss.html) |
| [**Lauren Barker**](/company/team/#laurenbarker) | Senior Fullstack Engineer | - Email: [lbarker@gitlab.com](mailto:lbarker@gitlab.com)<br>- GitLab: [@laurenbarker](https://gitlab.com/laurenbarker)<br>- Slack: @lbarker<br>- ReadMe: [lauren-barker](/handbook/marketing/readmes/lauren-barker.html) |
| [**Tyler Williams**](https://about.gitlab.com/company/team/#tywilliams) | Senior Fullstack Engineer | - Email: [tywilliams@gitlab.com](mailto:tywilliams@gitlab.com)<br>- GitLab: [@tywilliams](https://gitlab.com/tywilliams)<br>- Slack: @Tyler Williams<br>- ReadMe: [tyler-williams](/handbook/marketing/readmes/tyler-williams.html) |
| [**Mateo Penagos**](https://about.gitlab.com/company/team/#mpenagos-ext) | Senior Fullstack Engineer (Contract) | - GitLab: [@mpenagos-ext](https://gitlab.com/mpenagos-ext)<br>- Slack: @Mateo Penagos |
| [**John Arias**](https://about.gitlab.com/company/team/#jariasc-ext) | Senior Fullstack Engineer (Contract) | - Email: [jariasc-ext@gitlab.com](mailto:jariasc-ext@gitlab.com)<br>- GitLab: [@jariasc-ext](https://gitlab.com/jariasc-ext)<br>- Slack: @John Arias |
| [**Jessica Halloran**](https://gitlab.com/jhalloran) | Senior Product Designer (UX) | - GitLab: [@jhalloran](https://gitlab.com/jhalloran)<br>- Slack: @jhalloran |
| [**Tina Lise Ng**](https://about.gitlab.com/company/team/#tng) | Product Designer (UX) | - GitLab: [@Tinaliseng](https://gitlab.com/Tinaliseng)<br>- Slack: @Tina Lise Ng |
| [**Carrie Tsang**](https://gitlab.com/ctsang-ext) | Product Designer (Contract) | - Email: [ctsang-ext@gitlab.com](mailto:ctsang-ext@gitlab.com)<br>-  Slack: @ctsang-ext |
| [**Nathan Dubord**](https://about.gitlab.com/company/team/#ndubord) | Senior Frontend Engineer | - Email: [ndubord@gitlab.com](mailto:ndubord@gitlab.com)<br>- GitLab: [@ndubord](https://gitlab.com/ndubord)<br>- Slack: @Nathan Dubord<br>- ReadMe: [nathan-dubord](/handbook/marketing/readmes/nathan-dubord.html) |
| [**Laura Duggan**](https://about.gitlab.com/company/team/#lduggan) | Frontend Engineer | - Email: [lduggan@gitlab.com](mailto:lduggan@gitlab.com)<br>- GitLab: [@lduggan](https://gitlab.com/lduggan)<br>- Slack: @Laura Duggan<br>- ReadMe: [laura-duggan](/handbook/marketing/readmes/laura-duggan.html) |
| [**Javier Garcia**](https://about.gitlab.com/company/team/#jgarc) | Frontend Engineer | - Email: [jgarcia@gitlab.com](mailto:jgarcia@gitlab.com)<br>- GitLab: [@jgarc](https://gitlab.com/jgarc257)<br>- Slack: @Javi |
| [**Megan Filo**](https://about.gitlab.com/company/team/#meganfilo) | Frontend Engineer | - Email: [mfilo@gitlab.com](mailto:mfilo@gitlab.com)<br>- GitLab: [@meganfilo](https://gitlab.com/meganfilo)<br>- Slack: @Megan Filo<br>- ReadMe: [megan-filo](https://gitlab.com/meganfilo/meganfilo/-/blob/main/README.md) |
| [**Miguel Duque**](https://about.gitlab.com/company/team/#mduque-ext) | Frontend Engineer (Contract) | - Email: [mduque-ext@gitlab.com](mailto:mduque-ext@gitlab.com)<br>- GitLab: [@mduque-ext](https://gitlab.com/mduque-ext)<br>-  Slack: @Miguel Duque |

## Metrics

### North Star Metric

**Metric**: Conversion (past the [Pricing Page](https://about.gitlab.com/pricing/))

### Supporting Metrics

There are many other digital marketing metrics we track for the Marking Site. These include pageviews, unique visitors, time on site, and conversion metrics for key funnels. These detailed metrics are leading indicators for the health of our North Start metric, and are internal to GitLab.

## Scope

GitLab's digital marketing platform, or simply the “Marketing Site" refers to `https://about.gitlab.com` with the exception of the the [handbook](/handbook/).

# OKRs

We collaboratively define OKRs as a team with cross functional partners in advance of each quarter. Once OKR candidates are complete we review, size/scope them and align on which best help achieve our business objectives.

### Current Quarterly Plan

[FY23Q1 Digital Experience Quarterly Plan & OKRs](https://gitlab.com/groups/gitlab-com/marketing/digital-experience/-/epics/85)

# Our Process

We release every 2 weeks, always on a Wednesday. We can push MRs at any time but for collaborative work initiatives, we plan a package for delivery to ensure we’re consistently improving our prospective customer’s experience.

### Sprint Cycle

| Monday | Tuesday | Wednesday | Thursday | Friday |
| ------ | ------- | --------- | -------- | ------ |
| Sprint Begins |  |  |  |  |
|  |  | Sprint Release Async | Sprint Ends |  |

<figure class="video_container">
<iframe src="https://calendar.google.com/calendar/embed?src=c_g97ibfb3lq183mphm8mnbjfk34%40group.calendar.google.com&ctz=America%2FDenver" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>
</figure>

### Issue Board

- [Digital Experience Board](https://gitlab.com/groups/gitlab-com/marketing/digital-experience/-/boards/3288685)

### Sprint Planning

We use the following boards to plan sprints:
- [Current Sprint](https://gitlab.com/groups/gitlab-com/marketing/digital-experience/-/boards/3288486?iteration_id=Current)
- [DEX All](https://gitlab.com/groups/gitlab-com/marketing/digital-experience/-/boards/3288685)

Before a sprint starts, our team reviews potential work for the upcoming sprint. We identify issues that need to be written, write them, wieght them and add the to-do label.

A team member should enter sprint planning with an opinion on what the most important issues to work on are and be ready to make the sprint plan in collaboration with the team.

**Weighting**

We use issue weight to plan and manage our work. Team members are not held to weights, they’re simply a planning and learning tool. Digital Experience weights are based on the following point scale: 1pt = .5 day. In a full two-week sprint, we plan to deliver 20 points per team member.

### Weekly Check In

We use [Geekbot](https://geekbot.com/) to conduct asynchronous, weekly check-ins on iteration progress.

Each member of the Digital Experience team should be listed as a participant in the weekly check ins, and everyone should have permissions to manage the application for our team. The app can be configured through the [Geekbot Dashboard](https://app.geekbot.com/dashboard/), which you can visit directly, or find by clicking the **Geekbot** Slack conversation, navigating to the **About** tab, and clicking **App Homepage**.

### Code Reviews

Our team makes every attempt to complete [code reviews](/handbook/engineering/workflow/code-review/) on Merge Requests as timely as possible.

Team members who create a Merge Request should factor in a suitable amount of time for code and/or design review. If an issue has a due date, the MR creator should try have the work code-complete at least 24 hours prior to the intended release. This gives time for any major fixes that the reviewers may point out, and encourages quick [iterations](/handbook/values/#iteration) and [Minimal Viable Change releases](/handbook/values/#move-fast-by-shipping-the-minimal-viable-change).

When a team member is requested for review, it is good practice for them to post a comment in the Merge Request with an estimated timeline by which they expect to complete the review. For example, it is understandable to take 3 days to do a review, as long as you’ve let the MR creator know it may take that long. This gives the MR creator an opportunity to request a review from another team member.

Digital Experience code request reviews should include the [merge request checklist](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/.gitlab/merge_request_templates/Inbound-Marketing-Checklist-Basic.md) as referenced on the [reviewing merge requests](/handbook/marketing/inbound-marketing/digital-experience/website/merge-requests/) handbook page. Merge requests involving a URL redirect should also include the [redirect checklist](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/.gitlab/merge_request_templates/Inbound-Marketing-Redirect-Checklist.md).

### Figma Process

- [How we use Figma](https://about.gitlab.com/handbook/marketing/inbound-marketing/digital-experience/figma/)

### GitLab Product Process

From time to time, our team has objectives that require us to collaborate on the [GitLab product](https://gitlab.com/gitlab-org/gitlab). [Read more about the process for our engineers to onboard](/handbook/marketing/inbound-marketing/digital-experience/engineering-gitlab-product)

### Sprint Releases

On the second Wednesday of each sprint, we have a calendar reminder for: “Sprint Release Async”, which serves as a reminder for team members to add to our [Sprint Release Video Document](https://docs.google.com/document/d/1I9Th3Q-AakOkE_-pmNtEzwwMDSqYKF5Je2etGdPTovk/edit).

**Special cases during release post schedule:** we hold off on making changes to the `www-gitlab-com` repository during [release post](https://about.gitlab.com/handbook/marketing/blog/release-posts/#release-posts) days. The release post process is handled by a different team, and it can be disruptive to their work when we release changes to dependencies, CI/CD, or other major changes around their monthly release cadence.

### Sprint Retros

Sprint Retros allow us to reflect on what went well and what we can to do continuously improve our process.

This is the agenda we use for [Sprint Retros](https://docs.google.com/document/d/1kMNiUF2UDuSrMDuzLyRi8OEhVxry_MJoYi38RmmWafY/edit?usp=sharing)

### Sprint Videos

We release videos for: 
* [Sprint Planning](#sprint-planning) 
* [Sprint Releases](#sprint-releases) 
* [Sprint Retros](#sprint-retros) 

All videos are accessible in our [Digital Experience playlist on GitLab Unfiltered](https://www.youtube.com/playlist?list=PL05JrBw4t0KrakNGW0ruM5UL7DDlrMBba).

### Repository Health Day

At the end of every sprint cycle, Digital Experience will spend 10% or one day to work on issues related to improving the health of about.gitlab.com, the developer experience, tackle tech debt, or improve our documentation.

The structure of Repository Health Day is as follows: 1. Team members will choose what they wish to work on for this day. 1. Each team member will submit a single merge request to the `www-gitlab-com` repository by the end of repository health day. 1. This merge request will be related to an issue from any partner or group within GitLab.

By allowing our team members to contribute to the health of our repository for a day, we can contribute low-effort, high-impact solutions that will drive results for our team, partners, and the entire marketing site. This will enable Digital Experience team members to use their strengths to efficiently drive results for the `www-gitlab-com` repository. We’re all good at different things and come from different backgrounds. Let’s use that to our advantage to build a better repository that is inclusive of the team members that use it everyday.

### Repository Health Week

Digital Experience will dedicate one week or 8.33% per quarter to work on projects that improve the health of the `www-gitlab-com` repository, the developer experience, or tackle larger tech debt projects. These are projects that can not be completed in a single repository health day and require a higher degree of effort.

# Contact Us

### Slack Group

**@digital-experience** use this handle in any channel to mention every member of our team.

### Slack Channels

[#digital-experience-team](https://gitlab.slack.com/archives/CN8AVSFEY)

[#marketing](https://gitlab.slack.com/archives/C0AKZRSQ5)

[#website](https://gitlab.slack.com/archives/C62ERFCFM)

## GitLab Unfiltered Playlist

[*Digital Experience](https://www.youtube.com/playlist?list=PL05JrBw4t0KrakNGW0ruM5UL7DDlrMBba)

# Requesting Support

Our team works from a quarterly plan, for example: [FY22Q3](https://gitlab.com/groups/gitlab-com/marketing/inbound-marketing/-/epics/385) and [FY22Q4](https://gitlab.com/groups/gitlab-com/marketing/digital-experience/-/epics/3). Our quarterly plan is developed with the intention to put us 30% beyond our capacity which is [GitLab policy](https://about.gitlab.com/company/okrs/#okrs-are-stretch-goals-by-default).

We do our best to assist team members but do not operate as an internal agency so all requests will be prioritized against commitments in our current quarterly plan.

### Things we don't do

1. **Content changes**. You can do these yourself, it's one of the many awesome things about GitLab!
    1. Here's our documentation about how to use [GitLab's Web IDE](https://docs.gitlab.com/ee/user/project/web_ide/)
    1. Want to learn more about our CMS? [Here's the documentation](https://about.gitlab.com/handbook/marketing/netlifycms/)
2. **Create content**. You can collaborate with our excellent [Global Content team](https://about.gitlab.com/handbook/marketing/inbound-marketing/content/) for these needs.
3. **Create branded assets, custom graphics, illustrations**. Our [Brand team](https://about.gitlab.com/handbook/marketing/corporate-marketing/brand-activation/brand-design/) is so good at this, you definitely want their expertise.

### Issue template to submit an idea to drive our business goals

We love collaborating on work that drives our North Star and supporting metrics. If you have an idea, a strategic initiative, or an OKR that we requires our support here's how you can kick off our collaboration:

1. Review the FAQ section related to pre-work that will increase the chances your issue is prioritized.
2. Create an issue using [this template](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/-/issues/new#)

## Digital Experience FAQ

<details>
    <summary>Previous <b>Quarterly Plans & OKRs</b></summary>
    <li><a href="https://gitlab.com/groups/gitlab-com/marketing/inbound-marketing/-/epics/385">FY22Q3 Digital Experience Quarterly Plan & OKRs</a></li>
    <li><a href="https://gitlab.com/groups/gitlab-com/marketing/digital-experience/-/epics/3">FY22Q4 Digital Experience Quarterly Plan & OKRs</a></li>    
</details>
<details>
    <summary>Content Wireframe Instructions</summary>
    The Digital Experience team is primarily responsible for facilitating content, not creating it. Please prepare a content plan:
    <li>Provide the layout you think would work best from existing pages or existing blocks</li>
    <ul>
      <li>You can use the <a href="https://www.figma.com/file/nWIOpmuMp7RZXmfTj6ujAF/Slippers_foundations?node-id=1280%3A62">Blocks section in Slippers</a> to "shop" for blocks and plan your content to work with designs that already exist.</li>
      <li>This youtube video shows <a href="https://www.youtube.com/watch?v=HbnxXE4PT_s">how to navigate the Slippers Figma file</a> and how a content doc can work with the blocks.</li>
      <li>Not sure what a "block" is? You can read about that and more on our <a href="https://about.gitlab.com/handbook/marketing/inbound-marketing/digital-experience/digital-definitions/#blocks">Digital Definitions</a> handbook page.</li>
    </ul>
    <li>Provide the content in the layout of the existing block or page template</li>
    <ul>
      <li>Use this <a href="https://docs.google.com/document/d/1c2OYCtT57SACYSt55YMB5ZW-rt-JFw5RxYNWKPZt-Ao/edit?usp=sharing">Content + DSN Block Template</a> as a base. This template allows you to input <a href="https://www.figma.com/file/nWIOpmuMp7RZXmfTj6ujAF/Slippers_foundations?node-id=1280%3A62">existing block designs</a> mentioned above.</li>
      <li>This <a href="https://docs.google.com/document/d/1ypPJVNEpaSiafY0BnZ0p49tBfVeBGQnySVhgYE7gikk/edit?usp=sharing">Content Wireframe Template</a> is based on the current about.gitlab.com/enterprise page</li>
    </ul>

</details>
<details>
    <summary>Requesting homepage promotion</summary>
    <ul>
      <li>Use <a href="https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/new?issuable_template=request-website-homepage-promotion">this template</a> to request addition to the content calendar for homepage promotion</li>
      <li>If this is for a campaign, please put in the request as part of the initial campaign distribution plan</li>
      <li>For more information on this process click <a href="https://about.gitlab.com/handbook/marketing/inbound-marketing/content/#homepage-promotion-guidelines">here</a></li>
    </ul>
</details>
<details>
    <summary>Image Requirements</summary>
    <ul>
      <li>See <a href="https://about.gitlab.com/handbook/marketing/inbound-marketing/digital-experience/image-guidelines/">image guidelines</a> for specs</li>
    </ul>
</details>
<details>
    <summary>SEO Requirements</summary>
    <li>Know the URL and keywords you want to use</li>
    <ul>
      <li>SEO and keyword analysis from the Search Team <a href="https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/issues/new?issuable_template=keyword-research-request">Issue Templates</a> is recommended.</li>
      <li>See <a href="https://about.gitlab.com/handbook/marketing/inbound-marketing/digital-experience/website/#naming-conventions">naming conventions</a></li>
    </ul>
</details>
<details>
    <summary>Related Pages</summary>
    <ul>
      <li><a href="/handbook/marketing/inbound-marketing/digital-experience/data-dictionary/">Data Attribute Dictionary</a></li>
      <li><a href="/handbook/marketing/inbound-marketing/digital-experience/digital-definitions/">Digital Definitions</a></li>
      <li><a href="/handbook/marketing/inbound-marketing/digital-experience/engineering-ab-tests/">Engineering AB Tests</a></li>
      <li><a href="/handbook/marketing/inbound-marketing/digital-experience/engineering-marketo/">Engineering Marketo</a></li>
      <li><a href="/handbook/marketing/inbound-marketing/digital-experience/foundations-agenda/">Foundations Agenda</a></li>
      <li><a href="/handbook/marketing/inbound-marketing/digital-experience/writing-description-templates/">How to Write Templates</a></li>
      <li><a href="/handbook/marketing/inbound-marketing/digital-experience/image-guidelines/">Image Guidelines</a></li>
      <li><a href="/handbook/marketing/inbound-marketing/digital-experience/marketo-page-template/">Marketo Page Template</a></li>
      <li><a href="/handbook/marketing/inbound-marketing/digital-experience/slippers-design-system/">Slippers Design System</a></li>
      <li><a href="/handbook/marketing/inbound-marketing/digital-experience/video-bands/">Video Bands</a></li>
      <li><a href="/handbook/marketing/inbound-marketing/digital-experience/website/">Website</a></li>
      <li><a href="/handbook/marketing/inbound-marketing/digital-experience/figma/">Figma Process</a></li>
      <li><a href="/handbook/marketing/inbound-marketing/digital-experience/onetrust-cookie-consent/">OneTrust Implementation</a></li>
      <li><a href="/handbook/marketing/inbound-marketing/digital-experience/buyer-experience-repository/">Buyer Experience Repository</a></li>
    </ul>
</details>
<details>
    <summary>Checklists</summary>
    <ul>
      <li><a href="https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/.gitlab/merge_request_templates/Inbound-Marketing-Checklist-Basic.md">All Merge Requests</a></li>
      <li><a href="https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/.gitlab/merge_request_templates/Inbound-Marketing-AB-Checklist.md">AB Test MR</a></li>
      <li><a href="https://about.gitlab.com/handbook/marketing/inbound-marketing/content/editorial-team/#diversity-inclusion-and-belonging">Diversity, Inclusion, and Belonging</a></li>
      <li><a href="https://about.gitlab.com/handbook/marketing/inbound-marketing/digital-experience/website/merge-requests/#website-specific-reviews">Key page regression testing</a></li>
      <li><a href="https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/.gitlab/merge_request_templates/Inbound-Marketing-Redirect-Checklist.md">Redirect MR</a></li>
    </ul>
</details>
