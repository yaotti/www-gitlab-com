---
layout: handbook-page-toc
title: People Engineering Howtos
description: "Information on GitLab's email automation process related to People Group tasks."
---

## On this page

{:.no_toc}

- TOC
{:toc}

## Overview

People Operations team sends out several emails for different reasons. When possible, we try to automate these.

## Updating the onboarding email template

If changes are required to the onboarding email template, proceed with following the steps below:

- Browse to the MJML [page](https://mjml.io/try-it-live/)
- Open the MJML onboarding [template](https://gitlab.com/gitlab-com/people-group/people-operations/employment-templates/-/blob/main/email_templates/onboarding_email.mjml), as well as the html [version](https://gitlab.com/gitlab-com/people-group/people-operations/employment-templates/-/blob/main/email_templates/onboarding_email.html)
- Copy and paste the template into MJML website on the left
- Make the relevant changes and then select `View HTML` on the top left hand side of the website.
- Copy the HTML version to the HTML template.
- Copy and pasted the MJML version from the browser to the MJML template.
- Submit Merge Request like normal with the updates.