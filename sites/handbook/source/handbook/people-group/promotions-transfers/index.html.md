---
layout: handbook-page-toc
title: Promotions and Transfers
description: "Information and protocols related to GitLab promotions and transfers."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

Devloping talent internally is a key component of our success at GitLab, and our promotion process is built to support that development in alignment with our [values](https://about.gitlab.com/handbook/values/). Team members have two main avenues to pursue career advancement at GitLab: 1) Via our promotion process, and 2) By applying and interviewing for open positions that are approved in our headcount plan (transfers). 

Promotions are reviewed and approved once per quarter to support a consistent, predictable, and fair process. We leverage public (to GitLab) promotion documents where we outline the business results, increasing job scope, and values alignment that a team member demonstrates which support their promotion. The People Business Partner and leadership team are responsible for calibrating promotion nominations aligned with the timeline below. 

We encourage team members to take control of their own career advancement, and believe every team member deserves a great manager to support them. If you feel your title is not aligned with your skill level, come prepared with an explanation of how you believe you meet the proposed level and can satisfy the business need. Team members are empowered to own their development and can use the [Individual Growth Plan](https://about.gitlab.com/handbook/people-group/learning-and-development/career-development/#individual-growth-plan) as a tool to articulate and align on the skills they want to develop as they think about growing into a larger role. 

In addition to promotions, this page captures information about transfers, realignments and career mobility. 

## Definitions

- **Promotions** occur when a team member increases in level within the same job family. For example, a Backend Engineer is promoted to a Senior Backend Engineer. Similarly, a Senior Backend Engineer would receive a promotion by moving to a Staff Backend Engineer or Manager, Engineering.  
- **Transfers** occur when someone changes job families. A Backend Engineer would transfer to a Site Reliability Engineer. Generally these happen based on an open req and application process via Greenhouse. 
- **Manager Promotions** occur when someone is promoted to a people manager level. These promotions typically start with an open req and are processed through Greenhouse. 
- **Change in specialty** has no impact to job family. Therefore, this is not a promotion or a transfer.


# Promotion Philosophy

Our promotion philosophy is compromised of a few core pillars surrounding the approach and process alignment to our values. 

### Pillars

* Promote based on performance, not based on potential. Team members are already executing at the next level ([job grade](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades)) prior to promotion. 
* Career growth should be a partnership between team member and manager. As manager, it is important to create space to regularly discuss your team members' development and career aspirations, and identify opportunities to support them in their advancement and growth. If you as a team member feel you are approaching promotion readiness, it is encouraged for you to initiate this conversation with your manager. 
* When reviewing a proposed promotion, we should consider: 1) readiness of the individual, and 2) business need/opportunity
* All promotions at GitLab require a [promotion document](https://docs.google.com/document/d/1SavyZeQRXY4fOzv0Y7xMv4TiuWQIf51YtMptr_ZWFQs/edit#heading=h.5ahg1o4v53q7). We believe in [transparency](/handbook/values/#transparency) around the promotion process.
* We encourage team members to live our [efficiency value](/handbook/values/#efficiency) and be a [manager of one](/handbook/values/#managers-of-one) and take ownership of their promotion document in partnership with their manager.
* We calibrate promotions on a quarterly basis to ensure an equitable review, and through this process track metrics that help us understand if our promotions are occurring at a healthy and fair rate. 

### Values Alignment 

Our promotion philosophy is also aligned with our [values](/handbook/values/):

- **Collaboration**: Cross-functional lens for feedback and calibration 
- **Results**: Business justification, scope, and team member results are demonstrated and documented to support promotions
- **Efficiency**: Consistency and scalability in our processes
- **Diversity, Inclusion, and Belonging**: Fairness and equity reflected through a consistent approach and documentation to promotions at all levels in the organization and supported by our [job frameworks](/company/team/structure/) and Total Rewards [URG](/handbook/total-rewards/compensation/compensation-review-cycle/review-cycle-inputs/#underrepresented-group-audit) audit
- **Iteration**: Process is improved each cycle. In FY23, we moved to quarterly promotions with calibration and updated our processes and metrics accordingly.
- **Transparency**: Clarity and efficacy of promotion metrics, budget, and guidelines, in addition to transparency in promotion justification through internally public promotion documents.

## Promotion Process & Timeline

At GitLab, we promote on a quarterly cadence. This means that there is **one effective date per quarter when team members can be promoted**. There are three core stages to the promotion process: Planning, Calibration, and Processing. 

| Stage | Purpose |
| ---------- | ----- |
| [Planning](/handbook/people-group/promotions-transfers/#planning) | Managers and leaders to review their respective teams to determine promotion readiness, business need, and timeline for the upcoming quarters and project promotions |
| [Calibration](/handbook/people-group/promotions-transfers/#calibration) | The calibration exercise is an opportunity for leaders (sync or async) to review projected promotions on a quarterly basis. This is an opportunity to create visbility and ensure consistency in who we are promoting and why. |
| [Processing](/handbook/people-group/promotions-transfers/#process-hris-greenhouse-or-compaas) | The final stage once promotions are defined, is to determine where to process the promotion to finalize (this will take place via BambooHR, Compaas, or Greenhouse). 

Below is the timeline for FY23: 

- FY23-Q1:
  - Planning: December 15 to January 5
  - Calibrations: January 5 to January 15 
  - Processing: January 15 to February 1 
     - Promotions must be added to Compaas by January 20. 
     - Effective date for promotions: 2022-02-01. 
     - Communication: In conjunction with [annual compensation review raises](/handbook/total-rewards/compensation/compensation-review-cycle/#february). After communicated 1:1 with individuals, updates can be posted publicly in [#team-member-updates](/handbook/people-group/promotions-transfers/#bamboohr-promotion-approval-process).
- FY23-Q2: 
  - Planning: March 15 to April 1
  - Calibrations: April 1 to April 8
  - Processing: April 11 to May 1 
     - Promotions must be added to HRIS by April 20. 
     - Effective date for promotions: 2022-05-01. 
     - Communication: After fully approved in either Greenhouse or HRIS
- FY23-Q3: 
  - Planning: June 15 to July 1 
  - Calibrations: July 1 to July 8
  - Processing: July 11 to August 1
     - Promotions must be added to HRIS by July 20. 
     - Effective date for promotions: 2022-08-01. 
     - Communication: After fully approved in either Greenhouse or HRIS
- FY23-Q4:
  - Planning: September 15 to September 30
  - Calibrations: October 3 to October 10 
  - Processing: October 10 to November 1 
     - Promotions must be added to HRIS by October 20. 
     - Effective date for promotions: 2022-11-01. 
     - Communication: After fully approved in either Greenhouse or HRIS

_Please note that the Calibration timeline for Senior Director+ promotions will differ slightly from the timelines indicated above, as Senior Director+ promotions are calibrated at the [quarterly E-group offiste](https://about.gitlab.com/company/offsite/#schedule)._

### Quarterly Promotion Cadence Exceptions

While we are aligned to a quarterly promotion cadence across the organization, there are rare exceptions that will be considered. 
1. Application to a new, approved headcount in Greenhouse - internal candidates go through an interview process
2. Promotions stemming from individuals in [interim/acting roles](/handbook/people-group/promotions-transfers/#interim-and-acting-roles)

Approvals for the two scenarios above will be aligned with standard promotion approvals in BambooHR or Greenhouse.

If there is an exception requested that is _not aligned with one of the two exception scenarios above_, approvals should align with the following approval matrix:

| Promotion Level | Approvals Required |
| ---------- | ----- |
| Under Director level ([Job Grade](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades) 5-9.5) | 1) Direct Manager, 2) Department Head, 3) People Business Partner, 4) Total Rewards, 5) FP&A |
| Director+ level ([Job Grade](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades) 10-15) | 1) Direct Manager, 2) Department Head, 3) People Business Partner, 4) Total Rewards, 5) FP&A, 6) **E-Group Leader** |

The difference in approvals is the Director+ level off-cycle promotion exceptions require approval through E-Group, and off-cycle promotions for levels under Director require approval through Department head. 

Regardless of the promotion level, it is critical that leaders work with their People Business Partner, Total Rewards, and FP&A [as outlined](/handbook/total-rewards/compensation/compensation-review-cycle/#compensation-program-budget) to identify tradeoffs we can review to fund the promotion.



## Planning

Promotion Planning is generally done with via spreadsheets to maintain confidentiality and enable collaboration across department leaders. These spreadsheets are available to the Leaders of each department and are set up by the People Business Partners in collaboration with the Total Rewards team. Prior to the Planning phase in the timeline above, People Business Partners will make sure the spreadsheets are up to date before going into the Calibration phase. 

### Promotion Document

The [promotion document](https://docs.google.com/document/d/1SavyZeQRXY4fOzv0Y7xMv4TiuWQIf51YtMptr_ZWFQs/edit#heading=h.5ahg1o4v53q7) serves as a baseline to outline what should be included in all promotion documents, irrespective of division, department, or role. 

As the audience are other GitLab team members, the text should be written in third person using the team member's name and appropriate pronouns (he/she/they) to highlight the work and skills as evidence of the team member's suitability for the role.

Promotion documents should demonstrate values alignment, business need for the role, and team member readiness through delivery of impactful business results. The core sections in our promotion document are:
1. Promotion Summary
2. Values Alignment
3. Business Results
4. Business Justification 

When creating promotion documents, remember: 

- Promotions are based on performance, not potential 
- Promotion documents should not exceed 3 pages total.
- Please reference the [job frameworks](/company/team/structure/) in the handbook for guidance pertaining to expectations at the various levels at GitLab. The job levels should help guide data chosen to be included in the promotion document, in addition to discussion during calibration sessions.
- Please be sure that the promotion document has “comment” access enabled to ‘Anyone at GitLab who has the link!’ to ensure the review and approval process is not delayed. Please delete the instructions associated with each section of the promotion document below before submitting the promotions. 


### Promotion Metrics 

GitLab tracks the following promotion metrics in [Sisense](https://app.periscopedata.com/app/gitlab/756370/Promotion_Rate)

1. Internal Mobility

GitLab tracks Internal Mobility rate. Market data indicates that a 12% rolling promotion rate as the guideline for what we should see on average across the company for promotions. This is a guideline/target for business leaders to understand where they fall against what is considered standard in the market, not a restriction/cap. 

2. Average % Compensation Change

GitLab targets an average of [5-10% compensation change](/handbook/people-group/promotions-transfers/#recommend-a-compensation-increase) in general for promotions. This metric is in place to ensure we are consistent and equitable across the company when allocating promotion compensation raises to team members, in addition to ensuring competitive and meaningful promotion increases across the board. 

3. Budget Impact (see below) 

FP&A tracks budget impact by Department/Division on a quarterly basis


### Promotion Budget

Promotion budget is held at the division leader level, and optionally scaled down to department heads on a quarterly basis depending on department size. Decision to scale budget  down is at the division leader's discretion.

Please review the [Compensation Program Budget](https://about.gitlab.com/handbook/total-rewards/compensation/compensation-review-cycle/#compensation-program-budget) to understand how quarterly promotion budget is allocated and the process to review potential tradeoffs if divisions/departments are over/under budget for any given quarter. 

## Calibration

Each quarter Department/Division Leadership and the aligned People Business Partner plan a calibration session to review projected promotions. The goal of these calibration sessions is to set a fair and consistent standard in the Department for promotions, peer review promotions, and provide an opportunity for leaders to ask questions. These sessions can be async or sync. 

During calibration sessions, leaders should be prepared to discuss:

1. Core themes of the promotion document (values alignment, business justification, business results)
2. Improvement areas (the promotion document outlines strengths, but we also want to highlight how we will support a team member's development areas at the next level)
3. Cross-functional feedback (as our business goals and initiatives become increasingly cross-functional, managers should have a picture of how their team member collaborates effectively within their immediate teams, and with their core cross-functional partners and stakeholders)

Calibration should be aligned to the following levels of leaders and people managers:

| Promotion Level | Level Calibrated |
| ---------- | ----- |
| Under Director level ([Job Grade](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades) 5-9.5) | Calibrated at the Department level |
| Director level ([Job Grade](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades) 10) | Calibrated at the Division level |
| Senior Director+ level ([Job Grade](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades) 11-15) | Calibrated at the E-Group level |

Note that calibration structure may vary by division and department depending on size/scope/etc.

## Processing

There are three options for processing promotions: HRIS, Greenhouse or Compaas. Criteria to determine which option is most appropriate is outlined below.

## Promotions to Senior Director+

Philsophically, all promotions at GitLab are approached in the same way, follow the same high level process (Planning, Calibration, Processing), and use the same promotion document template. 

Promotions to Senior Director+ level ([job grade 11](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades) and above) have the following differences:

1. **Planning**: Senior Director+ promotions need to be added to E-group's promotion project sheet at least two quarters ahead of the desired promotion quarter for visbility. For example, if I want to promote an individual effective in Q1 (February), then I need to have this team member added to E-group's projection sheet within Q3 (latest October). Please work with your aligned People Business Partner, who will ensure the promotion projection is added. 
1. **Calibration**: All Senior Director+ level promotions are calibrated at the E-group level, as opposed to at the Department level. Calibration timeline will align to the timing of the [E-group offiste](https://about.gitlab.com/company/offsite/#schedule), and will thus differ from the Calibration timeline of the rest of the organization. All promotion documents need to be completed and shared with the E-group for visbility and preparation at least _2 weeks before the off-site date_. 


The only exception to this process is when filling an open Director and above role with an existing GitLab team member for a role posted publicly that an internal team member applies for. If external candidates have been considered and interviewed, and the internal candidate earns the role through a standard hiring process (screening, full interview process) then the recruiter may make an offer to the candidate as soon as the offer is approved. There should be no difference in the timing or process of making and accepting an offer for open roles between internal and external candidates.

### Greenhouse

- Promotions or Applications to Manager level roles: All managers will need to apply for the open position in [Greenhouse](/handbook/hiring/interviewing/#internal-applicants). They will go through the interview and approval process in Greenhouse to ensure anyone who would want to apply has the opportunity.
    - Example: Senior Backend Engineer applies to become a Manager, Engineering
- Interim Roles: Interim roles at all levels should go through the Greenhouse interview process (regardless of the number of applicants for the role).
    - Example: Senior Backend Engineer applies to become a Manager, Engineering (interim)
- Interim roles that transition to the permanent role: All team members that started the Greenhouse process and were hired into the interim role will make the transition to the permanent role via Greenhouse as well. While on the interim the req will be paused by Talent Acquisition and reopened to process the hire into the permanent role. 
    - Example: Manager, Engineering (interim) transitions to Manager, Engineering. 
- Lateral Transfers to a different job family: apply and approval through the Greenhouse [hiring process](/handbook/hiring/).
    - Example: Backend Engineer applies to a Site Reliability Engineering role.

#### Greenhouse Process Requirements

For any transfer being submitted through Greenhouse [hiring process](/handbook/hiring/), the following is required:

- The positions must be open for a minimum of 24 hours
- There must be a minimum of 2 interviews completed before moving to an offer for a candidate

### HRIS

- Promotions for Individual Contributors in the same job family: reviewed and approved through the [BambooHR Approval Process](/handbook/people-group/promotions-transfers/#bamboohr-promotion-approval-process)
    - Example: Backend Engineer is promoted to Senior Backend Engineer.
- Change in Territory, Segment, or Specialty:
    - Reviewed and approved through the [BambooHR Approval Process](/handbook/people-group/promotions-transfers/#bamboohr-promotion-approval-process) for any change in territory, segment, or specialty when there is no change to the job family or level, there is no compensation or stock adjustment and no backfill request.
    - Apply and approval through the Greenhouse [hiring process](/handbook/hiring/) for any change in territory, segment, or specialty that requires a change in job family or level, change in compensation or stock adjustment or requires a backfill request.

### Compaas

When the promotion cycle is running concurrently with our Annual Compensation review (FYXX-Q4) we will use Compaas for processing promotions to eliminate the need for two tools. The steps are as follows: 

- Work with your People Business Partner after the [Planning](/handbook/people-group/promotions-transfers/#planning) and [Calibration](/handbook/people-group/promotions-transfers/#calibration) to ensure Total Rewards has visbility into who is up for promotion. 
- Total Rewards will update the "slate" for the team member in Compaas to reflect the new compensation range aligned to the promoted level.

Compensation Recommendations:
1. Cash compensation
- Managers can make [their cash compensation increase recommendations via Compaas](/handbook/total-rewards/compensation/compensation-review-cycle/#determining-compensation-increases) aligned to the newly promoted level.

2. Equity compensation
- Total Rewards will input equity promotion grants into Compaas, managers do not need to do this. Managers **should not** consider the new promotion when making equity refresh recommendations for team members.

## Process for Managers: Requesting a Promotion or Compensation Change

To promote or change compensation for one of your direct reports, do the following steps:

1. Make sure to Plan and Calibrate on the promotion of the team member timely aligned with the above timeline. 
1. [Verify whether this promotion should be approved in BambooHR or Greenhouse](/handbook/people-group/promotions-transfers/#bamboohr-or-greenhouse-process). If BambooHR, continue. If Greenhouse, please reach out to the recruiter on the role.
1. [Create a Promotion or Compensation Change Document](/handbook/people-group/promotions-transfers/#creating-a-promotion-or-compensation-change-document)
1. If the team member you are recommeding a promotion for is director or above, please refer to the [promotions to director and above](https://about.gitlab.com/handbook/people-group/promotions-transfers/#promotions-to-director-and-above)
1. [Recommend a Compensation Increase](/handbook/people-group/promotions-transfers/#recommend-a-compensation-increase)
1. [Submit a request in BambooHR](/handbook/people-group/promotions-transfers/#submit-a-promotion-request-in-bamboohr)

**Things to consider before you start the process:**

- There will be situations when a team member is ready to move to the next level through a promotion, however, due to the nature of the business, that particular role or next level may not be available for business reasons. For example, the team member is ready for a Manager or Director role, however, the business does not have the need, budget or scope for an additional manager/director at that time. The position may or may not become available in the future but it is not a guarantee.
- If the vacancy is being advertised via the [jobs page](https://about.gitlab.com/jobs/) the individual must submit an application for the role in order to be compliant with global anti-discrimination laws. Similarly, if there is no vacancy posting, one must be created and shared on the `#new-vacancies` slack channel so that everyone has the opportunity to apply and be considered.

### Recommend a Compensation Increase

At GitLab, we ensure that promotions are impactful from the compensation perspective, stay within range of the compensation calculator, and are equitable to peers in the same role. As part of the BambooHR or Greenhouse request, the manager will be able to submit a recommended increase in cash compensation for their direct report using the compensation calculator. If you have any questions feel free to reach out to your People Business Partner or the Total Rewards Team.

#### Promotion Compensation Guidelines

- When a team member is promoted from one level to the next in the same job family, it is typical to be brought between the minimum and median of the compensation range.
- The Total Rewards Team typically recommends a 5%-10% increase as part of the promotion to cash compensation. Additional equity is fixed based on [stock options for promotions](/handbook/stock-options/#promotions).
    - Benchmarking data relays that typically [4-8% of your population will be promoted](https://www.shrm.org/ResourcesAndTools/business-solutions/Documents/Human-Capital-Report-All-Industries-All-FTEs.pdf) annually with an average of a [9.3% increase to base salary](https://www.worldatwork.org/docs/research-and-surveys/survey-brief-promotional-guidelines-2016.pdf).
- Any promotions with the following conditions will require additional justification to the Total Rewards team and executive approver. Please add the business justification as a comment in the BambooHR request.
    1. An increase of more than 10%
    1. The promotion exceeds the range in the compensation calculator (would be paid over the top end of the compensation range).

#### Transfer 

When reviewing compensation for a transfer in Greenhouse, the Total Rewards team will ensure internal equity among like roles ahead of approving the offer details using the following general guidelines: 

1. **Lateral Transfer (different or same job family, same grade)**: Typically we expect a team member to receive no increase or an increase in the amount of the benchmark differential for a lateral transfer. 
2. **Promotional Transfer (different or same job family, higher grade)**: We recommend a 5-10% increase (aligned to the promotional expected increase) and/or the difference in the benchmark for the transfer to a higher grade. 
3. **Other Transfer types**: There are other transfers that can be reviewed on a case by case basis. For example, if someone is transferring to a lower grade in a different or same job family, compensation may be adjusted down to ensure alignment to market rates for the role. Please tag the Total Rewards team in Greenhouse to conduct a review. 

### Submit a Promotion request in BambooHR as a manager

- Login to BambooHR.
- Select the team member you would like to adjust.
- On the top right-hand corner, click “Request a Change”.
- Select which type of change you are requesting.
    - Select “Compensation” if there is a salary change only
    - Select “Promotion” if there is a title change and a salary change involved.
    - Select “Job Information” if there is only a change in title
- When selecting the date, please note that People Connect Team will update the effective date to the beginning of the next applicable pay period after the promotion is fully approved.
- Enter in all applicable fields in the form, and then submit.
    - Note: The amount entered for the pay rate should be based on the number of pay periods for the team member. For example, if the team member is in the United States, the annual salary is divided by 24 to get the pay rate to be entered. If the employee is in the Netherlands, please divide by 12.96. The divisor can be found in the "Pay Frequency" field above "Comment for the Approver(s)" in the request form. For any questions on how to fill out the form, please reach out to People Ops Analysts.
- In the comments section please include:
    - The promotion document - the manager will ensure the promotion document is editable by those in the approval chain before submitting to the E-Group leader.
    - Any proposed change to variable compensation (if applicable)
    - If applicable, any business justification for an increase greater than 10% or above the compensation calculator.
- Once a promotion has been submitted, you can view the status of the promotion's approval from your [BambooHR Sent Requests](https://gitlab.bamboohr.com/inbox/sent_requests/) and navigating to the `Promotion` section. You will be asked within that process (and notified by your People Business Partner) to approve the recommended compensation changes before the process proceeds.
- Managers should not communicate any promotion or salary adjustment until the request has gone through the entire approval process and you receive an adjustment letter from People Connect Team.

### BambooHR Promotion Approval Process

This section describes the approval chain after a manager submits a promotion or compensation change request in BambooHR.

1. The indirect manager will review the promotion in BambooHR and will ensure the promotion document is editable by those in the approval chain before approving.
1. The People Business Partner (PBP) will then review the business case for promotion then they will ping the Total Rewards team to add the compensation notes (Review the [For People Business Partners](#for-people-business-partners-approving-promotions--compensation-changes) section below for details). The PBP will do a final check that the promotion document is editable by those in the approval chain before approving and submitting to the E-Group leader. 
1. The Total Rewards team will ensure the proposal adheres to the Global Compensation Calculator, with a comment outlining old compensation, new compensation, increase percentage, additional [stock options](/handbook/stock-options/#promotions), job code, rolling 12 month promotion rate for the department, if over the target for the rolling 12 month promotion rate also add the forward 12 month promotion plan for the department, and a link to the salary calculator. The Total Rewards team will also add the average compa ratio for the job code to serve as an additional data points for PBPs and managers. The People Business Partner will then approve the request if applicable.
1. The E-Group leader will review the entire proposal in BambooHR for approval. 
1. The CFO or CEO will review the request. If there are any questions, the CFO or CEO will add a comment outlining the specific concerns and the Total Rewards team will ensure follow-up within a week to escalate to the comp group or deny the request.
1. If the request is approved, the People Connect team will process the change on BambooHR and stage the [Letter of Adjustment](/handbook/people-group/promotions-transfers/#letter-of-adjustment) in DocuSign.
1. DocuSign will prompt the manager to discuss the promotion with the team member. The Manager will communicate the change to the team member in their `1-1 meeting by sharing the letter of adjustment on the call`. The Manager and the team member will process/sign the letter on the same call or any other scheduled call (in case of further disccussion about the promotion). Post acceptance by the team member, the Manager will announce the promotion on Slack **#team-member-updates** channel. In the announcment the manager will describe how the individual met the promotion criteria and include a link to the merge request where the individual's title is updated on the team page.
1. When announcing or discussing a promotion in the Slack **#team-member-updates** channel, *please include the link to the promotion Google Doc* to increase visibility for the reasons behind the promotion.
1. For change of departments or change of roles (individual contributor to managers), People Experience Associates will create a Career Mobility Issue.

### For People Business Partners: Approving Promotions & Compensation Changes

1. After the Planning and Calibrations People Business Partners make sure to notify Total Rewards of the upcoming promotions. 
1. Once added to BambooHR he People Business Partner (PBP) will receive an email titled `Promotion Request: [Team Member Name]`.
1. Confirm the [promotion document](/handbook/people-group/promotions-transfers/#creating-a-promotion-or-compensation-change-document) is included in the request.
1. Review the content of the promotion document.
    - Job family alignment
    - Values alignment
    - While not a requirement, some promotion documents also include: references from other team members and improvement areas
    - Check the sharing settings, as this should be readable by everyone at GitLab with a link
1. Review Bonuses received by the team member.
1. Ping the manager requesting the promotion via Slack to confirm that the department leader and E-Group leader are aware and supportive of the promotion.
1. Align with the Total Rewards in the private #promo_compensation Slack channel to ensure Compensation notes are added in BambooHR.
1. Cross-check Compensation adjustment in accordance with the [compensation calculator](https://comp-calculator.gitlab.net/users/sign_in).
1. We want for promotions and compensation changes in Engineering that they are approved by all leaders up to the CTO. As BambooHR has a limit in the amount of approvers use the Division specific slack channel to have approval of all leaders.
1. If an approver is skipped in BambooHR: Share the promotion information in the private Division-specific promotion Slack channel with an overview of:
    - Team member name
    - Promotion request (current job title and new job title)
    - Compensation notes (current compensation, proposed compensation, increase %, additional stock options, compensation calculator)
1. `@mention` the skipped department leader for approval in the Division-specific promotion Slack channel. Example: Promotion for Backend Engineer to Senior Backend Engineer. The BambooHR process includes the direct manager, department Director and CTO. The missing department leader here is the VP of Development. In this case `@mention` the VP of Development for approval.
1. PBP approves the promotion request in BambooHR.

### For People Connect Team: Processing Promotions & Compensation Changes

1. If the request is approved through BambooHR, the People Connect team will create the [Letter of Adjustment](/handbook/people-group/promotions-transfers/#letter-of-adjustment), whereas if the request is through Greenhouse the `People Experience Team will be notified via the People Experience team email` inbox that the [Letter of Adjustment](/handbook/hiring/talent-acquisition-framework/ces-contract-processes/#letter-of-adjustment) has been created by the CES team and signed. If this is the case, only data systems will need to be updated.
1. If the request comes through BambooHR, approve the request, then update the entries in BambooHR to ensure that there are the proper dates, amounts, job information. Also, ensure to add stock options to the job tab if applicable. If the team member is moving to a Manager position, update their access level in BambooHR.
1. Notify Payroll of the changes. This can be done in the following google docs: United States: "Payroll Changes", Everyone else: "Monthly payroll changes for non-US international team members". Payroll does not need to be notified for Contractors.
1. If the team member is in Sales or transferred to Sales, update the changes tab on the "Final Sales OTE FY 2020" google doc.
1. If the team member is employed in Canada notify Total Rewards of the salary change (so they can update their salaries on Canada Life and Collage platforms).

#### Letter of Adjustment

1. For all the [GitLab entities](https://about.gitlab.com/handbook/people-group/employment-solutions/#gitlab-entities) and [Independant Contractors](https://about.gitlab.com/handbook/people-group/employment-solutions/#team-members-hired-as-contractors-via-our-gitlab-it-bv-entity) create letter of adjustment as per the steps mentioned below. For [PEO's](https://about.gitlab.com/handbook/people-group/employment-solutions/#peo-professional-employer-organization-employer-of-record-and-not-a-gitlab-entity), check if a notification to the PEO is required for the creation of letter of adjustment. Currently only Remote Technology requires a notification to generate the letter of adjustment for their respective team members.  
1. Make a copy of the [Letter of Adjustment template](https://docs.google.com/document/d/12AscsmP3qnTqCC9h6cx1UpIVrnZUfoN6AvfMjf8guyg/edit) and enter all applicable information based on the BambooHR request. The effective date is as follows:
    - For sales personnel with a variable change, the effective date is always the 1st of the month regardless of their entity.
    - For US team members, the effective date should be either the 1st or the 16th. If the [payroll cut off date](/handbook/finance/payroll/#payroll-cut-off-date) has passed for the current pay period, the effective date should be made for the start of the next pay period. The GitLab Inc and Federal Payroll calendar should be referenced when determining the effective date.
    - For example, if the change is being processed on June 22, since this date is before the payroll cut off date of June 23, the effective date should be June 16.
    - If the change instead is being processed on June 25, the effective date should be July 1 since this is after the payroll cut off date.
    - For Canada team members, the effective should be the start of the pay period closest to, but not after the [payroll cut off date](/handbook/finance/payroll/#payroll-cut-off-date) depending on when the change is processed. The GitLab Canada Corp Payroll calendar should be referenced when determining the effective date.
    - For example, if the change is being processed on June 15, since the payroll cut off date of June 6 has passed, this would go to the next pay period with cut off date of June 20. The corresponding start of the pay period for the June 20 cut off date is June 21 so June 21 should be the effective date.
    - For all other changes, the effective date should be the first of the current month if processed on or before the 8th of the month and the first of the next month if processed after the 8th of the month.
    - For example, if a GitLab Ltd team member has a change being processed on June 7, this would be effective June 1.
    - If the change was instead being processed on June 15, this would be effective July 1.
1. Stage the letter in DocuSign and add the following team members to sign:
    - Add radio button (Delete the additional one) for the `Total Rewards Analyst` to audit
    - Add signature field for the Total Rewards Authorized Signatory
    - Add radio button (Delete the additional one) for the Manager to communicate the change to the team member by sharing the letter of adjustment during the 1:1 Zoom call and then again add one radio button to (Delete the additional one) announce on the `#team-member-updates` Slack channel. 
    - Add signature field for the team member
    - Add sign date field for the team member
    - **Note:** Make sure that **a)** "Set signing order” option has been selected while preparing the doc, and **b)** Select radio button instead of checkboxes as only radio button allows you to select the required field/mandatory field option. This prohibits the Total Rewards Analysts and Manager to process the letter without checking the tasks on the letters.
1. Once signed by all parties, save the letter to the “Contracts & Changes” folder in BambooHR.
1. If some amount of onboarding in the new role or offboarding from the old role is required (for example a change in access levels to infrastructure systems; switch in groups and email aliases, etc.), notify People Experience Associates in the internal Promotions/Transfers spreadsheet tracker (using the `people-exp@gitlab.com` alias) and the People Experience Associates will create an associated [Career Mobility Issue](https://gitlab.com/gitlab-com/people-group/people-operations/employment-templates/-/blob/main/.gitlab/issue_templates/career_mobility.md) with the [Slack Command](/handbook/people-group/engineering/#internal-transition-issue-creation) list and track associated tasks for the previous and new manager. Tag or mention the aligned [People Business Partner](/handbook/people-group/#people-business-partner-alignment-to-division) in the Career Mobility Issue.
1. The previous manager will be prompted to create an [Access Removal Request Issue](https://gitlab.com/gitlab-com/team-member-epics/access-requests/blob/master/.gitlab/issue_templates/Access%20Removal%20Request.md) and the new manager will create an [Access Request Issue](https://gitlab.com/gitlab-com/team-member-epics/access-requests) to ensure the correct access is given for the new role and deprovisioned for the previous role, if need be.

**Note** Letter of adjustment is sent to team member's personal email address instead of GitLab email address to ensure a proper audit trail.  

## Interim and Acting Roles

### Interim (Engineering-Specific)

As part of the career development structure within the Engineering division, interim and acting role opportunities occasionally arise. For more information on how interim and acting roles fit into Engineering career development, please reference the [Engineering career development handbook page](/handbook/engineering/career-development/). For information on the interim and acting processes, please continue reading below.

#### Beginning Interim Period

As highlighted in the Defition section, all interim roles (regardless of the number of applicants) should go through the Greenhouse application and interview process. The interview process steps will be determined by the hiring manager and next level leader. This will contain several steps of the standard [GitLab hiring process](https://about.gitlab.com/handbook/hiring/interviewing/#typical-hiring-timeline). The process for team members interested in applying for an interim role is as follows:

1. _Team Member_: Apply for the interim position in Greenhouse.

_Once a team member successfully completes the interview process and is selected for the interim period, the following steps should be taken to ensure the team member is set up for success in their interim role._

1. _CES_: Issue a [Letter of Adjustment](/handbook/hiring/talent-acquisition-framework/ces-contract-processes/#letter-of-adjustment) to finalize the beginning of the interim period. The Letter should include: interim job title, start date, and end date (if known). Letters of adjustment are important as this is the process by which Total Rewards is notified of change from Greenhouse.
1. _People Connect Team_: Update the team member's job title in BambooHR to reflect that they have started an interim role (I.E. `Senior Manager, Engineering (interim)`). This update serves as the SSOT for tracking interim start and end dates, in addition to providing transparency pertaining to who is currently executing in an interim role. Job code and job grade will remain the same, as interim periods have no impact on compensation.
1. _Current Manager_: A [Job Information Change Request](https://about.gitlab.com/handbook/people-group/promotions-transfers/#job-information-change-in-bamboohr) should be submitted to:
    - Move interim direct reports so the team member's interim direct reports are reporting to them in BambooHR. The change needs to be initiated by the current manager. The philosophy here is that if a team member has successfully gone through the interview process and has demonstrated they are ready/able for an interim period in a manager role, they have the required level of EQ and discretion to have direct reports in BambooHR. It is, of course, expected that should the interim period not end in promotion, the team member continue to treat confidential information confidentially.

#### Ending Interim Period

When the interim period comes to a close, one of two outcomes can occur:

1. The team member successfully completes the interim period aligned with [the success criteria](https://about.gitlab.com/handbook/engineering/career-development/#interim-manager) and moves into the interim role permanently.

- As a general guideline, the interim period should last no _less_ than 30 days, and no _more_ than 4 months .
- The hiring manager should submit a formal offer through Greenhouse including the promotion document to make the change official. The accomplishments leading up to the interim and during the interim can be used for the promotion document.

1. The team member does not complete the interim period successful or decides that the manager track is not something they want to pursue, and moves back to their role prior to the interim period.

- A feedback session between the team member and hiring manager should take place, so it is clear to the team member why the interim period was not successful.
- The manager should submit a [job information change request](https://about.gitlab.com/handbook/people-group/promotions-transfers/#job-information-change-in-bamboohr) in BambooHR to revert the team member's job title.
- Not successfully completing the interim period _does not_ mean the team member can not move into a similar role in the future

Irrespective of the outcome, when the interim period ends, the manager should review the [Criteria For Eligibility](/handbook/total-rewards/compensation/#criteria-for-eligibility) for the [Interim Bonus](/handbook/total-rewards/compensation/#calculation-of-interim-bonus) and [submit an interim bonus request](/handbook/total-rewards/compensation/#submitting) for the team member.

### Acting

A person "acting" in the role is someone who occupies a role temporarily and will move back to their original role after a set amount of time or other conditions. "Acting" in a role may be experimenting with the role as a part of determining an individual's career development path, or may be filling in for a vacant role while we hire someone to fill the role permanently. While interim is only applicable to the Engineering division, acting is used across GitLab.

_Interviews are not required role Acting roles as they generally do not end in promotion, nor are direct reports in BambooHR generally moved to Acting managers._ The process for selecting someone for an acting position is:

- Upcoming Acting roles will be discussed over Staff meetings.
- Leadership can gather interest from their team members for the upcoming acting roles.
- The hiring manager will determine the most suitable team member for the acting role.
- *Please make sure that the department head is in the loop and supportive of the acting period and candidate selected before moving forward.*

_When the acting period ends, the manager should review the [Criteria For Eligibility](/handbook/total-rewards/compensation/#criteria-for-eligibility) for the [Interim Bonus](/handbook/total-rewards/compensation/#calculation-of-interim-bonus) and [submit an interim bonus request](/handbook/incentives/#process-for-recommending-a-working-group-for-a-bonus-in-bamboohr) for the team member._

### Updating Acting or Interim Movements in BambooHR
If you recieve a job change or letter of adjustment to an interim or acting role here is how to process the change. 
1. Save the letter to Documents > Contracts folder - `Share with employee`
1. Review for compensation change 
1. Access Level 
    - Acting roles - No access change
    - Interim roles - Access changed (if required)
1. Update Job title (if required)
1. Update Manager (if required)
1. Update [Acting/Interim Tracker Spreadsheet](https://docs.google.com/spreadsheets/d/1Uoz0frqDYEDINeodeeKBGMFVzdTuoe4x63LpHxU9WMo/edit#gid=0)
1. Update [Transition Tracker Spreadsheet](https://docs.google.com/spreadsheets/d/10O0idnLKcE4MHaNSt9dcvRzTx0Q_G6klKcznvmgrJbY/edit#gid=1366346015) (if required) 

## Demotions

To demote one of your direct reports, a manager should follow the following steps:

- The manager should discuss any performance issues or possible demotions with the People Business Partner in their scheduled meetings with a corresponding google doc.
- To initiate the process, the manager must obtain agreement from two levels of management.
- Proposed changes to a current vacancy description or a new vacancy description should be delivered with request for approval by the second level manager and the People Ops Manager.
- Demotions should also include a review of [compensation](/handbook/total-rewards/compensation/) and [stock options](/handbook/stock-options/#stock-options) in the google doc. Managers should consult with Total Rewards team on these topics; and of course always adhere to the Global Compensation Calculator.
- Once agreement is reached on the demotion and changes (if any) in compensation, the total rewards team will act as the point of escalation to have any demotion reviewed and approved by the Compensation Group once the relevant google doc is complete.
- Once approved, the manager informs the individual. Please cc people-exp@ gitlab.com  once the individual has been informed, to processes the changes in the relevant administrative systems, and stage a [Letter of Adjustment](https://docs.google.com/document/d/12AscsmP3qnTqCC9h6cx1UpIVrnZUfoN6AvfMjf8guyg/editm).
- Communication should be on a need-to-know basis only and should not be made public out of respect for the individual. 
- The manager will initiate any necessary access requests or access change requests. 

## Job Information Change in BambooHR

Job information changes are anything that requires an update to the team member's profile in BambooHR that is not compensation related. The current manager is the person who needs to submit all job information change requests (including requests to change team member's manager).

**Process for the current manager:**

1. Login to Bamboo HR and select the direct report
1. In the Request Change dropdown, select Job information
    - Under "Effective Date" input the date the transfer to the new manager is effective.
    - Under "Department" input the new (if applicable) Department
    - Under "Reports To" search and select the name of the Hiring Manager
    - Under "Comment for Approvers" paste any additional relevant information.
    - Update the `Job Title Specialty` field (if applicable)

### For People Connect: Processing Job Information Change Requests

1. Audit the team member's department, division and cost center against the new manager's.
1. For US team members, the entity must also match with the manager's. For any changes in entity, update the payroll file.
1. In case of `Job Title Specialty` change requests. Notify People Experience Associates in the internal Transition Tracker spreadsheet tracker (using the `people-exp@gitlab.com` alias) and the People Experience Associates will create an associated [Career Mobility Issue](https://gitlab.com/gitlab-com/people-group/people-operations/employment-templates/-/blob/main/.gitlab/issue_templates/career_mobility.md) with the [Slack Command](/handbook/people-group/engineering/#internal-transition-issue-creation) list and track associated tasks for the previous and new manager. Tag or mention the aligned [People Business Partner](/handbook/people-group/#people-business-partner-alignment-to-division) in the Career Mobility Issue.

## Department Transfers

If you are interested in a vacancy, regardless of level, outside your department or general career progression, you can apply for a transfer through [Greenhouse](https://boards.greenhouse.io/gitlab) or the internal job board, link found on the #new-vacancies Slack channel.
 We have no minimum time in role requirements for transfers at GitLab.

**If and when you decide to officially apply for an internal position, please make sure your current manager is aware. Your official application is signaled by you applying to the open role or reaching out to the talent acquisition team. Informal conversations about the role do not require a team member to inform their manager. If you have concerns about communicating your interest in an internal role to your manager, please reach out to your People Business Partner.**

### For Internal Applicants

#### Different Job Family

- If you are interested in a transfer, simply submit an application for the new position. If you are not sure the new role is a good fit, schedule time with the hiring manager to learn more information about the role and the skills needed. If after that conversation you are interested in pursuing the internal opportunity, it is recommended that you inform your current manager of your intent to interview for a new role. While you do not need their permission to apply to the new role, we encourage you to be transparent with them. Most will appreciate that transparency since it's generally better than learning about your move from someone reaching out to them as a reference check. You can also use this as an opportunity to discuss the feedback that would be given to the potential new manager were they to seek it regarding your performance from your current and/or past managers. We understand that the desire to transfer may be related to various factors. If the factor is a desire NOT to work with your current manager, this can be a difficult conversation to have and shouldn't prevent you from pursuing a new role at GitLab.
- Transfers must go through the application process for the new position by applying on the [jobs page](https://gitlab.greenhouse.io/internal_job_board). The team member may go through the entire interview process outlined on the vacancy description. Common exceptions to the standard interview process are behavioral or "values alignment" stages. The Recruiter will document the reason behind alterations to the standard interview plan in the team member's Greenhouse profile.  If you have any questions about the role or the process, please reach out to your Department or Division's [People Business Partner](/handbook/people-group/#people-business-partner-alignment-to-division). In all cases, the applicable People Business Partner should be informed via email, before a transfer is confirmed.
- In the case of transfers, it is expected and required that the gaining manager will check with internal references at GitLab limited to the previous and current managers; please do not conduct internal reference checks with peers or direct reports. For questions or exceptions, please engage your recruiter and people business partner.
- It is recommended (but not required) that the applicant, current manager, or gaining manager create a private Slack channel to help coordinate the transfer. Invite anyone who will be involved such as the relevant managers, directors, people business partners, finance business partners, and recruiters for the current team and gaining team.
- If the current manager needs to backfill the role in Engineering they should follow [this process](/handbook/engineering/#rd-backfill--transfer-process). For other divisions they should work with their department leader, recruiter, and the Finance Business Partner to confirm that a backfill is available. When the transfer is confirmed, current manager should work with recruiter and Finance Partner to obtain a [GHP ID](/handbook/finance/financial-planning-and-analysis/#headcount-and-the-talent-acquisition-single-source-of-truth) for the backfill and open the role in Greenhouse.
- Before the offer is made the recruiter will confirm with the team member and the gaining manager that they have indeed reached out to the current manager. They will discuss the new internal opportunity and that an offer will be made to the team member.
- Talent Acquisition team will ensure that, if applicable, the position has been posted for at least three business days before an offer is made.
- [Compensation](/handbook/total-rewards/compensation/) and [stock options](/handbook/stock-options/#promotions) may be reviewed during the hiring process to reflect the new level and position.
- If after interviews, the manager and the GitLab team-member want to proceed with the transfer, internal references should be checked. While a manager cannot block a transfer, there is often good feedback that can help inform the decision. It is advised that the GitLab team-member talk to their manager to explain their preference for the new team and to understand the feedback that will be given to the new manager. It should also be noted, that performance requirements are not always equal across roles, so if a GitLab team-member struggles in one role, those weakness may not be as pronounced in the new role, and vice versa. However, if there are systemic performance problems unrelated to the specific role or team, a transfer is not the right solution.
- The Recruiter and Hiring Manager will review the offer details with the internal candidate and a [Letter of Adjustment](/handbook/people-group/promotions-transfers/#letter-of-adjustment) will be sent out by the Total Rewards team following the hiring process
- If the team member is transferred, the new manager will announce in the `#team-member-updates` Slack Channel and begin any additional onboarding or offboarding necessary. Before the new manager makes the transfer announcement they must confirm with the team members current manager that the current team has been informed about the team members new position and transfer.
- Team members changing functional roles should complete onboarding for the new function. For example, a Backend Engineer who transferring to become or work on Frontend work should do Frontend Engineer onboarding.

#### Same Job Family, Different Department or Specialty

If the team member is staying in the current benchmark for the Job Family, but changing their Specialty or Department (ex: moving from Plan to Secure or moving from Development to Infrastructure), the above steps will be followed. Solely the recruitment procedure might be shortened if the requirements for the role are the same. At a minimum we would ask for the hiring manager to have an interview with the team member.

If selected for the role, a [Letter of Adjustment](/handbook/people-group/promotions-transfers/#letter-of-adjustment) will be sent by the Total Rewards team outlining the changes to department and specialty for the Total Rewards team to process in BambooHR. If the current manager needs to backfill the role they should reach out to the Finance Partner.

### Internal Department Transfers

If you are interested in another position within your department and the manager is also your manager you must do the following;

- Present your proposition to your manager with a google doc.
- If the vacancy is advertised on the [jobs page](/jobs/), to be considered, you must submit an application. If there is no vacancy posting, one must be created and shared in the #new-vacancies channel so that everyone has the opportunity to apply and be considered.
- The manager will asses the function requirements; each level should be defined in the vacancy description.
- If approved, your manager will need to obtain approval from their manager, through the chain of command to the CEO.
- [Compensation](/handbook/total-rewards/compensation/) and [stock options](/handbook/stock-options/#stock-options) will be reevaluated to ensure it adheres to the compensation calculator. Don't send the proposal to the CEO until this part is included.
- If the team member is transferred, the manager will announce in the `#team-member-update` Slack channel and begin any additional onboarding or offboarding necessary.

## Internal Transfer Start Date

If a GitLab team-member is chosen for a new role, the managers should agree on a reasonable and speedy transfer plan. Up to 4 weeks is usually a reasonable period, but good judgment should be used on completing the transfer in a way that is the best interest of the company, impacted people, and projects.

The agreed upon transfer date should be reflected in the offer letter in Greenhouse to ensure transfer date alignment between current manager, new manager, and transferring team member.  This is essential to ensure that both teams are able to plan and be set up for success. It is important to note that it is typically the _former_ team of the team member that shuffles to ensure workload of the departing team member is covered while a backfill is hired for. This is to ensure a smooth and speedy transfer and a positive team member experience. When aligning on a start date, please also considering [payroll alignment](/handbook/people-group/promotions-transfers/#letter-of-adjustment) when selecting a start date. 

Delaying transfers should be avoided to ensure a good team member experience for the transferring individual, however, if due to extenuating circumstances a transfer date needs to be pushed out, both managers need to agree on a new transfer date to communicate to the team member. 

## Department Transfers Manager Initiated

If you are a manager wishing to recruit someone, the process is the same as a team member-initiated transfer. 

* We highly encourage the hiring manager to be transparent with the team member's current manager. Doing so will allow the current manager the maximal amount of time to plan for the transfer and speed up the overall process.
* The hiring manager should post the open role in internal public forums to attract all potentially interested candidates.
* It is highly discouraged to reach out to a potential candidate directly without talking to their manager first. The hiring manager should share their intention of reaching out to the current manager's team member to ensure transparency and awareness. 

#### When promotion is a consideration - Within Same Job Family

If a team member sees a vacancy posted that is the next level up within their [job family](/handbook/hiring/job-families/#job-families) (for example an Intermediate Frontend Engineer sees a vacancy for an Senior Frontend Engineer), the team member should have a conversation with their manager about exploring that opportunity. Once that discussion is completed the team member should follow the internal department transfers guidance above.

It is the manager’s responsibility to be honest with the team member about their performance as it relates their promotion readiness. If the manager agrees that the team member is ready, then they will be promoted to the next level. If they do not think the team member is ready for the promotion, they should walk through their career development document, as well as work on a promotion plan with the team member. The manager should be clear that the team member is not ready for the promotion at this time and what they need to work on. If the team member would still like to submit an application for the role after the conversation with their manager, they can apply and go through the same interview process as external candidates. The recruiter will confirm with the manager that the promotion readiness conversation has taken place before the internal interview process starts.

#### For Internal Applicants - Different Job Family

If the role is in a completely different job family (within their own division or in a completely different division, for example, if a Product Designer is interested in a Product Manager role), the team member must submit an application via the posting on GitLab’s [internal job board](https://app2.greenhouse.io/internal_job_board) on Greenhouse.

After the team member applies, the recruiter will reach out to the team member to connect regarding compensation for the role. In some cases, the compensation may be lower than the current one. Once the team member understands and agrees with the compensation for the new role, they can continue the interview process.

Internal and external candidates will have the same process with the same amount of interviews and when possible the same interviewers, with the exception of the full screening call (which will be instead a short conversation to discuss compensation, as mentioned above). However, if the internal applicant will be staying in the same division and the executive level interview is a part of the process, the executive may choose to skip their interview. All interview feedback and notes will be captured in the internal team member’s Greenhouse profile, which will be automatically hidden from the team member. After interviews are completed, internal “reference checks” will be completed with the applicant’s current manager by the new hiring manager.

It is recommended that team members inform their manager of their desire to move internally and their career aspirations. Your manager should not hear about your new opportunity from the new hiring manager; it should come from you prior to the new hiring manager checking in for references with the current manager.

If you are unsure of the role, set up a coffee chat with the hiring manager to introduce yourself. Express your interest in the role and your desire to learn more about the vacancy requirements and skills needed. If after that conversation you do not feel that you are qualified or comfortable making the move, ask the hiring manager to provide guidance on what you can do to develop yourself so you will be ready for the next opportunity. It may also be possible to set up an [internship for learning](/handbook/people-group/learning-and-development/career-development/#internship-for-learning) situation with the hiring manager.

#### Announcing Internal Promotions/Transfers

While the [Career Mobility Issue Issue](https://gitlab.com/gitlab-com/people-group/people-operations/employment-templates/-/blob/main/.gitlab/issue_templates/career_mobility.md) aims to kick off the logistics of switching roles, the guidelines below are meant to guide the communication of internal promotions and transitions to ensure consistency and alignment from all parties involved.

1. Prior to any company-wide announcement, the team member should be given the opportunity to share the news with their immediate team members.
1. The `new manager` should post the announcement in the `#team-member-updates` Slack channel. This should ideally happen (timezome permitting) on the <b>same day that the candidate signs their [Letter of Adjustment](/handbook/people-group/promotions-transfers/#letter-of-adjustment)</b>.
1. For cases where announcing on the same day the Letter of Adjustment is signed is not possible, the announcement should no more than 24 hours after the candidate has signed.
1. Following this initial announcement, the `current manager` can proceed with making this announcement in other relevant team-specific channels.

_NOTE: Though the Total Rewards and People Experience team may have visibility into promotions or transfers due to the administration of updating information as part of their roles, they should not communicate with the team member about their promotion/tranfer until an announcement has been made._--

#### Realignment of team members impacting multiple teams

Company priorities can change and occasionally some or all members of a team may be asked to transfer to high priority vacancies within other teams.

##### Realignment Considerations

Early and ongoing communication with affected teams is key to ensuring a successful realignment. Hiring managers should clearly articulate the business need and quantifiable benefits from the realignment, as well as position a team focus and roadmap that gives affected teams an understanding of how their future contributions will benefit GitLab.

Consideration should be given to impacts on [product category maturity](/direction/maturity/) commitments for teams with departing team members.

Relevant items that can assist include:

* Links to Team handbook page outlining goals and vision
* Roadmap, issue backlog, and links to key Epics.
* Staffing allocation plans
* Expected impact or success criteria

When possible, realignments should respect the [Product Development Timeline](https://about.gitlab.com/handbook/engineering/workflow/#product-development-timeline) to allow impacted teams to complete existing work.

##### Realignment Process

In cases where multiple individuals are asked to transfer to high priority roles:

1. Legal counsel and the People Business Partner for the group should be notified to align on the process and impact of the team member realignment.
1. Select a DRI to coordinate the overall realignment process. This DRI should work closely with their People Business Partner to ensure we comply with all local labor laws.
1. Document the realignment decision in a confidential issue with details such as impacted parties and reasoning for realignment.
1. Communicate the reassignment decision to effected team members. Emphasize this is not about poor performance, but rather a way to shift high value individuals to the highest priorities.
1. Organize one or more Team Pitch Office Hours meeting where individuals considering transfers can learn about teams that are hiring. Hiring managers should attend the office hours to talk about what is interesting about their teams. [Team Pitch Office Hours video](https://www.youtube.com/watch?v=-MgiUA7sAHU&feature=youtu.be)
1. Encourage individuals considering transfers to meet with hiring managers to get more information about the roles they are interested in.
1. Ask for and record the role each individual's transfer preference. Also ask for their second choice and third choice.
1. After individuals gave their preference, the skills/requirements of the roles will be matched to the skills of the individuals. For example: level, product/technical skills, potentially soft skills.
1. DRI work with People Business Partner to ensure all legal requirements are met as these vary between countries.
1. Ask hiring manager to approve transfer. If they don't approve look at the individual's first or second choice.
1. Once choices are finalized, approved, and rationale is documented, current manager should communicate the decision directly with individual impacted team members.
1. Current manager follows the standard process for [updating job information in BambooHR](/handbook/people-group/promotions-transfers/#job-information-change-in-bamboohr). _Before communicating company-wide, all changes should be reflected in BambooHR._
1. Move the confidential issue to the [Product project](https://gitlab.com/gitlab-com/Product), and label it with ~"realignment".
1. Product Leader announces the realignment in #product (for smaller realignments) or #company-fyi (for larger realignments)
1. The new manager follows the steps outlined for[announcing transfers](/handbook/people-group/promotions-transfers/#announcing-internal-promotionstransfers) in the handbook.
1. After the realignment the DRI opens a retrospective issue to gather feedback from affected teams in order to improve this process.
1. After the realignment the DRI makes the realignment issue public and closes it. This is because only planned realignments are [not public](https://about.gitlab.com/handbook/communication/#not-public). In cases where the issue does contain public information the DRI may choose to leave the issue confidential with a comment explaining the reason.

#### For People Success & Talent Acquisition Team

Vacancies will be posted internally using the Greenhouse internal job board for at least 3 business days. If a role is not posted internally there must be a business case documented in the HRIS file of the team member who received the new role. See [Department Transfers](/handbook/people-group/promotions-transfers/#department-transfers) for additional details.

More details can be found on the [Internal Applications](/handbook/hiring/interviewing/#internal-applicants) page and the [Letter of Adjustment](/handbook/people-group/promotions-transfers/#letter-of-adjustment) section.

### Leveling Up Your Skills

There are a number of different ways to enhance or add to your skill-set at GitLab, for example, if you do not feel like you meet the requirements for an inter-department transfer, discuss with your manager about allocating time to achieve this. This can be at any level. If you're learning to program, but aren't sure how to make the leap to becoming a developer, you could contribute to an open-source project like GitLab in your own time.

As detailed in our [Learning & Development Handbook](/handbook/people-group/learning-and-development/), team members will be supported in developing their skills and increasing their knowledge even if promotion is not their end goal.

## Career Mobility Issue

A [Career Mobility Issue](https://gitlab.com/gitlab-com/people-group/people-operations/employment-templates/-/blob/master/.gitlab/issue_templates/career_mobility.md) is created when the one of the following criteria is met;

- Migration from Individual Contributor to Manager
- Migration from Manager to Individual Contributor
- Migration of Team

When a career mobility may not be needed (but can be requested);
- Team/Speciality change but no access request needed

## Career Mobility Issue Creation Process

The People Connect Team will notify the People Experience Team of a pending migration of a team member via @ mention in the Promotion/Transfer Tracker. The [People Experience Associate](https://about.gitlab.com/job-families/people-ops/people-experience-associate/) currently in the assignment rotation will assign the migration to an Associate in the People Exp/Ops Tracker.

The [Career Mobility Issue](https://gitlab.com/gitlab-com/people-group/people-operations/employment-templates/-/blob/master/.gitlab/issue_templates/career_mobility.md) will then be **created by the People Experience Associate** assigned by using the [automated Slack command](/handbook/people-group/engineering/employment-issues/#career-mobility-issues) three days prior to the effective date to allow for the managers to start preparing for the team members transition.

Important things to ensure:

1. Add a due date of two weeks from the migration effective date.
1. Check to see that the previous Manager and new Manager is listed correctly in the issue.
1. Complete all applicable tasks under the People Experience list.

## Important Tasks once Career Mobility has been finalised

1. This needs action from both the current and new managers to set the migrating team member up for success in their new role. This may include:

- Creating the correct Access Requests for systems needed and for systems no longer needed.
- Create any training issue that may be required.
- Reminding the team member to update their title on the team page, on their GitLab profile, in Zoom, in Slack and on professional networks like Linkedin. If relevant, remind them to order new business cards as well.
- If we are in the middle of company wide 360 reviews, it is encouraged that the current manager and new manager arrange a successful handover of the feedback, whether sync or async.

## Career Mobility Retrospective

The team member going through this transition and assigned to their Career Mobility issue have a set of tasks to complete. An important one is to create a retrospective thread within their Career Mobility issue, so that they and their respective previous and current managers can discuss any questions, comments, proposals and more about their issue within said issue. Retrospectives are used in many ways at GitLab, such as which are used after GitLab product [releases](/handbook/communication/#release-retrospectives-and-kickoffs) and describing the Product [retrospective workflow](/handbook/engineering/workflow/#retrospective). For the Career Mobility issue, simply comment in the issue, starting a thread titled **Retro thread** or **Retrospective**. Please feel free to ping your assigned People Experience Associate in your issue if you have any questions.

## Compliance

The People Experience Associate completes a bi-weekly audit of all career mobility issues that are still open and checks that all tasks have been completed by all members applicable. In the event that tasks are still outstanding, the People Experience Associate will ping the relevant team members within the transition issue to call for tasks to be completed.

Once all tasks have been completed and the issue is still open, the People Experience Associate will close the transition issue accordingly. The transitioning team member can also close the issue once satisfied all the tasks are complete.

All migration tasks by the applicable team members needs to be completed within 2 weeks of the migration start date.

## For Team Members: Transitioning To A New Manager

There are several situations at GitLab that could lead to team members changing managers, including promotions, lateral transfers, company restructuring, manager resignation, etc. The process of building a new relationship with a new manager can be uncertain at times, but there are resources to help this transition process go smoothly:

- [Transitioning 1-1s](/handbook/leadership/1-1/#transitioning-1-1s) is a very important part of manager transitions. This helps ensure the new manager is up to speed on important discussions, deliverables, etc. so this information does not get lost in the transition.
- The [career mobility template](https://docs.google.com/document/d/1iRbmS518CDjmhZEjvkaibqR1g0angWE83sWvtlH-elE/edit#heading=h.5ahg1o4v53q7) helps to ensure accomplishments, strengths, development areas, etc. are all captured with evidence moving forward. This makes sure career development progress continues and is not lost with manager changes. 
- Sharing your most recent 360 feedback review and most recent performance review with your new manager can also be a great way to align on your strengths and improvement areas and discuss how they can partner with you in developing both.
