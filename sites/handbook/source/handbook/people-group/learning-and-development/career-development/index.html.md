---
layout: handbook-page-toc
title: Career Development and Mobility
description: "It is the lifelong process of managing learning, work, leisure and transitions in order to move toward a personally determined and evolving preferred future."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## What is Career Development

Career development is the lifelong process of managing learning, work, leisure and transitions to move toward a personally determined and evolving preferred future.

On 2021-08-18 Samantha Lee and Jacie Bandur on the Learning & Development team had a handbook learning session with Eric Johnson, CTO, about career development. 

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/hYahDZ9nIWA" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

## Key Career Resources for Team Members

Check out some of the key programs offered for team members to grow their career at GitLab.

<div class="flex-row" markdown="0">
  <div>
    <a href="/handbook/total-rewards/benefits/general-and-entity-benefits/#growth-and-development-benefit" class="btn btn-purple" style="width:250px;margin:5px;"> Growth and Development Benefit </a>
    <a href="/handbook/people-group/learning-and-development/career-development/#tracking-your-career-development" class="btn btn-purple" style="width:200px;margin:5px;">Goal Tracking Resources</a>
         <a href="/handbook/people-group/learning-and-development/career-development/ic-development" class="btn btn-purple" style="width:200px;margin:5px;">Programs for Individual Contributors</a>
  </div>
</div>

<div class="flex-row" markdown="0">
  <div>
      <a href="/handbook/people-group/learning-and-development/career-development/#having-career-development-conversations" class="btn btn-purple" style="width:250px;margin:5px;"> Career Development Conversations </a>
      <a href="/handbook/people-group/learning-and-development/career-development/#skill-of-the-month" class="btn btn-purple" style="width:200px;margin:5px;">Skill of the Month</a>
      <a href="/handbook/people-group/learning-and-development/career-development/#skill-of-the-month" class="btn btn-purple" style="width:200px;margin:5px;">Mentorship</a>
  </div>
</div>

<div class="flex-row" markdown="0">
  <div>
    <a href="/handbook/people-group/learning-and-development/career-development/#internship-for-learning" class="btn btn-purple" style="width:200px;margin:5px;">Internship for Learning</a>
    <a href="/handbook/people-group/learning-and-development/career-development/#recorded-workshops" class="btn btn-purple" style="width:200px;margin:5px;">Recorded Workshops</a>
  </div>
</div>

## Connecting Career Development to Wellbeing

Watch this live speaker series where we talk to Leah Weiss of [Skylyte](https://www.skylyte.com/) about the connection between wellbeing, purpose, and our career growth.

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/2XmmGYlsEP8" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

## Roles and Responsibilities

**Team Member Owned**
 1. Take charge of your own development and career
 1. Close the skill gap for current and future positions
 1. Seize development and stretch opportunities
 1. Remember there are no promises or guarantees of a promotion

**Manager Facilitated**
1. Take time with team members to discuss their career aspirations
1. Listen and provide feedback, ideas and contacts
1. Make choices to support their development

**GitLab Supported**
1. Communicate future direction and skills requirements
1. Provide information and tools when applicable
1. Communicate internal opportunities

**70-20-10 Rule for Development**
1. 70% of your development should come from on-the-job and action learning.  This can include development experiences like managing a project, serving on a cross-functional team, taking on a new task, job shadowing, job rotation, etc.
1. 20% of your development should come from interactions with others.  This includes having a mentor, being a mentor, coaching, participating in communities of practice, serving as a leader in your organization, etc.
1. 10% of your development should come from training, including classes, seminars, webinars, podcasts, reading, conferences, etc. Here is an example of the Enablement stage's process for applying for the [professional development budget](/handbook/engineering/development/enablement/processes/budget_approval.html).

**Additional Questions to Think About:**
1. Do you have any overused strengths or underdeveloped skills that might cause your career to stall or derail?
1. Considering feedback from others, are you perceived to have the skills required for the business needs of the future? If not, how could you shape that perception in a favorable direction?
1. How can you leverage your current skills and talents for your future aspirations?
1. What skills or talents are missing to qualify you for your future aspirations?
1. Does your feedback from others tell you anything about how feasible your aspirations are?
1. Do you currently have the skills and talents needed for the future business needs?  If not what can you do now to get ready?

## Career Development versus Career Mobility

#### The Relationship between learning and [promotions](/handbook/people-group/promotions-transfers/)

As is highlighted in our [Leadership](/handbook/leadership/1-1/#key-points) section, GitLab team members should not feel pressure to climb the proverbial ladder. We recognize that not everyone wants to advance or move to a new level, and that is supported. Developing one's skills and promotion at the company are not mutually exclusive.

As detailed in GitLab's [Definition of Diversity, Inclusion & Belonging ](/company/culture/inclusion/), we recognize that unique characteristics and experiences form how we as individuals approach challenges and solve problems. They also shape how we view success in our individual careers and lives. Not everyone views promotion as a measure of success, and team members will not be thought less of or penalized for holding this view.

As part of GitLab's [Transparency](/handbook/values/#transparency) value, team members are encouraged to be open and honest with their manager. You are encouraged to learn and develop your skills without pressure to in turn seek promotion. If you feel you are not being supported in this way, please visit the [Need Help?](/handbook/people-group/#reach-peopleops) portion of the People Group Handbook.

#### Career Development

Career development involves the process of choosing a career and building a career path that will accelerate professional growth. It is perfectly acceptable to seek out learning and development opportunities — to sharpen one's understanding of a coding language to better understand a function, etc. — yet not strive for promotion beyond your current role. Some team members are happier and more productive without managing a team, for example.

**Team member examples of career development:**

1. An engineer takes a LinkedIn Learning course to improve their communication and leadership skills
1. A project manager participates in a mentorship program practice their goal setting and planning skills
1. A team member on any team takes a LinkedIn Learning course becuase the topic is of personal interest

#### Career Mobility

Career mobility involves both lateral (promotions) and horizontal (transfers) movement along a career path. This mobility most often involves applying actions taken and skills developed as leverage for a new role. Career mobility requires action and reflection after the learning is complete to demonstrate gained skills and transfer them to a new role or responsibility set. 

We use the [career mobility issue template](/handbook/people-group/engineering/career-mobility/) to guide team members when they transfer or are promoted to a new role within the organization.

**Team member examples of career mobility:**

1. An engineer takes a leadership course and volunteers as a mentor working towards their goal of becoming an engineering manager
1. A project manager earns a professional certificate as they work towards a senior-level promotion
1. A sales team member completes an internship with marketing to explore new career opportunities in a different division

## Internal Resources

### Growth and Development Benefit

Review the total rewards handbook for details on how to use the [GitLab Growth and Development benefit](/handbook/total-rewards/benefits/general-and-entity-benefits/#growth-and-development-benefit) for GitLab team members.

Some external resources that might be qualified for expenseing under this benefit include:

1. [Stanford's Centre for Professional Development](http://scpd.stanford.edu/home)
1. [Yale Open Courseware](https://oyc.yale.edu/)
1. [MIT Open Courseware](https://ocw.mit.edu/index.htm)
1. [Notre Dame Open Courseware](https://www.edx.org/school/notredamex)
1. [WorkLife with Adam Grant Podcast](https://www.ted.com/series/worklife_with_adam_grant)
1. [Dose of Leadership with Richard Rierson - Authentic & Courageous Leadership Development](https://www.stitcher.com/podcast/dose-of-leadership-podcast)

### Recorded Workshops

During our Career Development workshops at Contribute, May 2019 we took team members through tips to creating a clear growth (aka development) plan. Below are the resources from that session:

1. [Career Development Workshop, slides](https://docs.google.com/presentation/d/1yY0ofMGgzN07ylTAnRP5geFnWcgUYkiVlcIyR54tpD0/edit#slide=id.g29a70c6c35_0_68)
1. [Individual Growth Plan template](https://docs.google.com/document/d/1ZjdIuK5mNpljiHnFMK4dvqfTOzV9iSJj66OtoYbniFM/edit)
1. [Tips for Creating Effective Growth Plans](https://docs.google.com/document/d/1O45gRkQqUa3dEgjJXGwdBE7iZbBI22EPC7zrkS3T4dM/edit)
1. [Talent Assessment template](https://docs.google.com/document/d/1XT1l1STUBilDdVGHXlCcmSE5ApxwVqO1Ln4Wk8e1Fpk/edit)
1. [Individual Development Plan in Security](/handbook/engineering/security/individual-development-plan.html)

### Internal Opportunities to expand exposure

There are various internal opportunities to expand a team member's exposure to multiple parts of the organization.  These include:

1. Participation in a [Team Member Resource Group (TMRG)](h/company/culture/inclusion/erg-guide/)
1. Work on a [Working Group (WG)](/company/team/structure/working-groups/)
1. Do an [apprenticeship](/handbook/engineering/career-development/#internship-for-learning) on another team
1. Apply to do the [CEO shadow program](/handbook/ceo/shadow/) for two weeks
1. When appropriate and possible, attending some of the staff meetings of the team members manager's manager

### Internship for Learning

If your manager has coverage, you can spend a percentage of your time working (through an 'internship') with another team.

This could be for any reason: maybe you want to broaden your skills or maybe you've done that work before and find it interesting.

If your team has someone working part-time on it, it's on the manager of that team to ensure that the person has the support and training they need, so they don't get stuck. Maybe that's a buddy system, or maybe it's just encouragement to use existing Slack channels - whatever works for the individuals involved.

#### How does this work?

**What percentage of time should be allocated?** Well, 10% time and 20% time are reasonably common. As long as you and your manager have the capacity the decision is theirs and yours.

**What about the team losing a person for X% of the time? How are they supposed to get work done?**
Each manager needs to manage the capacity of their team appropriately. If all of the team are 'at work' (no one is on PTO, or parental leave, or off sick), and the team still can't afford X% of one person's time - that team might be over capacity.

**Can I join a team where I have no experience or skills in that area?**
That's up to the managers involved. It may be that the first step is to spend some time without producing anything in particular - in which case, it's possible that the [Growth and Development Benefit](/handbook/total-rewards/benefits/general-and-entity-benefits/#growth-and-development-benefit) may be a better fit (or it might not.)

**How long does an internship of this nature last?**
This will vary from team to team, but typically 6 weeks to 3 months depending on the goals for your internship.

**This sounds great but how do I initiate this process?**
First step is to discuss this with your manager at your next 1:1. Come prepared with your proposal highlighting what skills you want to learn/enhance and the amount of time you think you will need. Remember, this should be of benefit to you and GitLab. You and your manager will need to collaborate on how you both can make this happen which may also involve discussing it further with the manager of the team you may be looking to transfer to. All discussions will be done transparently with you. Be mindful though that the business needs may mean a move can't happen immediately.

**How do I find a mentor?**
On the [team page](/company/team/), you can see who is willing to be a mentor by looking at the associated [expertise](/company/team/structure/#expert) on their entry.

**Does completing an internship guarantee me a role on the team?**
Completing an internship through this program does not guarantee an internal transfer. For example, there may not be enough
allocated headcount in the time-frame in which you complete your internship.

If at the end of your internship, you are interested in transferring teams please follow the guidelines in [Internal Department Transfers](/handbook/people-group/promotions-transfers/#internal-department-transfers).

#### Starting Your New Internship

Please create a new issue in the [Training project](https://gitlab.com/gitlab-com/people-ops/Training/issues/new), choose the `internship_for_learning` template, and fill out the placeholders.

No internship for learning should be approved without both managers having a conversation and agreeing upon the percentage of time the team member will be spending on the internship. Also, the team member's manager has discretion not to approve the internship for learning if there are team member performance issues.

Once you've agreed upon the internship goals, both managers should inform their respective groups' People Business Partner.

#### Recommendations

We recommend that, at any given time, each [team](/company/team/structure/#team-and-team-members) is handling only one intern. This is to allow for an efficient and focused mentorship without impacting the capacity of the team. You can, of course, adjust this depending on the size of the team but please consider the impact of mentoring when scheduling internships.


### Skill of the Month

In August 2021 we held a Career Development Skill of the Month. All items are linked in the [FY22 Skill of the Month](https://gitlab.edcast.com/channel/skill-of-the-month-fy22) GitLab Learn Channel. All of the Async AMA Issues can be found in the [Career Development Interviews 2021 Epic](https://gitlab.com/groups/gitlab-com/people-group/learning-development/-/epics/68). More will be added to this section as we go through the weeks of the month. 

#### Week 1 

**Topic: Career Development for GitLab Team Members**

1. [Career Development with Pattie Egan](https://gitlab.edcast.com/pathways/career-development-with-pattie-egan) 
1. [Career Development with Darren Murph](https://gitlab.edcast.com/pathways/career-development-with-darren-murph) 

#### Week 2 

**Topic: Individual Growth Plan** 

1. [Career Development with Nicolas Dular](https://gitlab.edcast.com/pathways/career-development-with-nicolas-dular )
1. [Career Development with Rebecca Spainhower](https://gitlab.edcast.com/pathways/career-development-with-rebecca-spainhower )
1. [Building an Individual Growth Plan](https://gitlab.edcast.com/pathways/building-an-individual-growth-plan) 
1. [Using the Individual Growth Plan Interview](/handbook/people-group/learning-and-development/career-development/#gitlab-team-member-using-the-igp) 
1. [Using the Achievement Tracker](/handbook/people-group/learning-and-development/career-development/#using-your-accomplishment-tracker) 
1. [Using Epics to Track Career Development](/handbook/people-group/learning-and-development/career-development/#use-gitlab-epics-to-track-your-career-development) 
1. Live Session: Individual Growth Plan: [Recording of call](/handbook/people-group/learning-and-development/career-development/#igp-live-learning)

#### Week 3 

**Topic: Having Career Conversations with Your Manager**

1. [Career Development with Brittany Rohde](https://gitlab.edcast.com/pathways/career-development-with-brittany-rohde)
1. [Career Development with Christopher Wang](https://gitlab.edcast.com/pathways/career-development-with-christopher-wang)
1. [Career Development with Brendan O'Leary](https://gitlab.edcast.com/pathways/career-development-with-brendan-o-leary)
1. Live Session: Having Career Conversations with Your Manager: [Recording of call](/handbook/people-group/learning-and-development/career-development/#having-career-development-conversations-with-your-manager)

#### Week 4 

**Topic: Having Career Conversations with Your Team Members**

1. [Career Development with Diana Stanley](https://gitlab.edcast.com/pathways/ECL-3b8242ee-8583-4dbd-80b1-e337d812d406)
1. Career Conversations with your Team
   - [Eric Johnson](https://www.youtube.com/watch?v=hYahDZ9nIWA)
   - [Darva Satcher](https://www.youtube.com/watch?v=F7br2CqkjGE)
   - [Jane Gianoutsos](https://www.youtube.com/watch?v=PA3Cb51aNrA)
1. [Career Development Framework interview with Kenny Johnston](https://www.youtube.com/watch?v=dNZjmf68UIg)
1. Live Session: Having Career Conversations with Your Team Members: [Recording of call](/handbook/people-group/learning-and-development/career-development/#having-career-development-conversations-with-your-team)

#### Week 5

1. [Career Development with Sherrod Patching](https://gitlab.edcast.com/pathways/career-development-with-sherrod-patching)


## Tracking your Career Development

### Individual Growth Plan 

The [Individual Growth Plan (IGP)](https://docs.google.com/document/d/1ZjdIuK5mNpljiHnFMK4dvqfTOzV9iSJj66OtoYbniFM/edit) is a great way to take initiative of your career growth. 

If you aren't sure where to start or are having trouble filling out your IGP, we recommend watching the [How to Develop your Career Plan](https://www.linkedin.com/learning/how-to-develop-your-career-plan/introduction?u=2255073) course on LinkedIn Learning. 

Steps: 
1. Open the [Individual Growth Plan (IGP) Template](https://docs.google.com/document/d/1ZjdIuK5mNpljiHnFMK4dvqfTOzV9iSJj66OtoYbniFM/edit)
1. Make a copy of the template + save to your drive
1. Fill out the template 
1. Review with your manager 
1. Start working towards your goals! 

#### GitLab Team Member Using the IGP

The video below is an interivew with a GitLab team member who has used the Individual Growth Plan in their career development. 
<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/0ih0lMoKxN0" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

#### IGP Live Learning

The video below is a recording of the Live Learning that took place in August 2021 as part of our Career Development [Skill of the Month](/handbook/people-group/learning-and-development/learning-initiatives/#fy22-topic-outline). 
<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/dJezH0ToqLk" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

### Tracking your Accomplishments 

Some people may call it a "brag sheet". Some call it a CREDIT Tracker. Either way, it's a great place to put all of your accomplishments and positive feedback. We encourage you to utizlize the Accomplishment tracker in conjunction with the IGP and GitLab Epics. 

Steps: 
1. Go to the [Accomplishment Tracker Template](https://docs.google.com/document/d/1tBjEbX-p4MYinEsdU-91KDQ_BJkihwrBKZ0n9yfdLjo/edit?usp=sharing)
1. Make a copy + save to your drive
1. Start filling it out (check out the video below for a walkthrough)

#### Using your Accomplishment Tracker
<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/p4a60slPg5k" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

#### Tracking informal feedback

It also can be very helpful to capture feedback from team members. For example, you might want to preserve feedback from slack since these messages are only [retained for 90 days](/handbook/communication/#slack). The easiest way to do this is to save screenshots to a separate directory on your Google drive.

### Health Tracker: Growth Plan

Consider making a copy of this [Google sheet template](https://docs.google.com/spreadsheets/d/1Pr_yl8TRAXucSr4qYml6TdsO86PgiaKB/edit#gid=2104005676) to track the current status and long term goals you've set for both your personal and professional growth.

### Use GitLab Epics to track your career development

Epics are a great way to [transparently](/handbook/values/#transparency) and [efficiently](/handbook/values/#efficiency) track your work and map learning back to your [results](/handbook/values/#results).

Here are two examples of using Epics to organize career development work from the Learning and Development team: [Example 1](https://gitlab.com/groups/gitlab-com/people-group/learning-development/-/epics/43), [Example 2](https://gitlab.com/groups/gitlab-com/people-group/learning-development/-/epics/47)

Below is a short video from the L&D team reviewing how and why you might choose to use epics for career development tracking:

<iframe width="560" height="315" src="https://www.youtube.com/embed/xuisSgBQtaU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

1. Open an epic in a personal or team project
1. Use the description to highlight your learning goals, ideal programs or resources you want to review, considerations when planning, etc.
1. Open issues to track actions you'll take as a result or inspired by things learned from the resources you've used
1. Add those issues as related to your epic. If you can't add the related issues because they are in different projects, consider starting a new thread in the epic that links to any and all issues that are a result of, or impacted by, your career development work

### Career Mapping and Development
{:#career-mapping-and-development}

We have started this process at GitLab by defining Junior, Senior and Staff advancement levels. Career Mapping helps GitLab team members to understand and develop the skills they need to achieve their goals, giving them clear criteria.
Mapping helps managers and leaders internally develop the skills and knowledge they need to achieve future business goals. The key to this is to identify the key skills, knowledge, and abilities needed to master each level. Another essential tool is a career development plan, here are some examples:

1. [Career Plan](https://docs.google.com/document/d/1hJIzMnVhEz3X4k24oAwNnlgGhBeQ518Cps9kLVRRoWQ/edit)
- [Template - Development Scorecard](https://docs.google.com/spreadsheets/d/1DBrukzzsV6InaCkZf8_ngLeTcLQ9uj6ynE93qLmHkQA/edit#gid=1677297587)
1. [Career Plan Template](https://performancemanager.successfactors.com/doc/po/develop_employee/carsample.html)

Managers should discuss career development at least once a month at the [1:1](/handbook/leadership/1-1/) and then support their team members with creating a plan to help them achieve their career goals. If you would to know more about this please checkout the [career mapping course video](https://www.youtube.com/watch?v=YoZH5Hhygc4)


## Having Career Development Conversations

### Having Career Development Conversations with your Manager

The video below is a recording of the Live Learning that took place in August 2021 as part of our Career Development [Skill of the Month](/handbook/people-group/learning-and-development/learning-initiatives/#fy22-topic-outline). 

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/JA3NNTGoELU" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

### Having Career Development Conversations with your Team

The video below is a recording of the Live Learning that took place in August 2021 as part of our Career Development [Skill of the Month](/handbook/people-group/learning-and-development/learning-initiatives/#fy22-topic-outline). 

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/f1T2KKlrZCY" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

#### Recomendations for Managers to support the learning of team members

Managers should support their direct reports with career and professional development goals. Managers can help direct reports by implementing the following into their [1-1](/handbook/leadership/1-1/), team meetings, and async communications. 

1. Encourage team members to incorporate learning and professional development into their [Individual Growth Plan](/handbook/people-group/learning-and-development/career-development/#internal-resources-1) or using [Gitlab Epics to track career development](/handbook/people-group/learning-and-development/career-development/#use-gitlab-epics-to-track-your-career-development). 
1. Utilize the [Growth & Development Benefit](/handbook/total-rewards/benefits/general-and-entity-benefits/growth-and-development/) for professional development opportunities
1. Ensure that direct reports prioritize learning, and support different learning styles: 
     - Recommend blocking time to take [time out to learn](/handbook/people-group/learning-and-development/learning-initiatives/#take-time-out-to-learn-campaign). This can be on a weekly, monthly, or quarterly basis; whatever works best for the reports learning goals. 
    - Organize communities of learning within your team and cross-functional ones to encourage [social learning.](/handbook/people-group/learning-and-development/learning-initiatives/#social-learning-through-live-learning)
    - Be the example for your team by sharing career and professional development pursuits you are currently looking into and completing in synchronous and asynchronous communications. 
1. Create a team or functional [Learning Hub](/handbook/people-group/learning-and-development/work-with-us/#creating-a-learning-hub-for-your-team) on [GitLab Learn](/handbook/people-group/learning-and-development/gitlab-learn/) to organize team learning paths and activities to promote self-service and self-paced learning. 
    - Curate content in GitLab by reviewing offerings in [LinkedIn Learning](/handbook/people-group/learning-and-development/linkedin-learning/), the Handbook, external vendors, and open-source material that apply to the role and skills needed for the team. 
    - Document learning in a centralized knowledge base in GitLab Learn and a Handbook page. 

### Career Development Conversation Acknowledgements

Career development is a key factor in team member engagement and role satisfaction. As part of the FY'20 GitLab annual [engagement survey results](/handbook/people-group/engagement/) it was clear that team members want to have meaningful conversations with their managers on an annual basis or even more frequently. Starting in FY'22 we will be tracking career development conversations via BambooHR. 

### Cadence

This process is just an acknowledgement by the team member that they have had career conversations. Aligned with the Performance/Potential [matrix cadence](/handbook/people-group/performance-assessments-and-succession-planning/#regular-cadence), formal career conversations will ideally happen twice per year:

* Once in Q2
* Once in Q4

The bi-annual cadence is not required, but recommended. Some team members may prefer annual career conversations, other team members may prefer more informal check-ins quarterly. This is a personal decision that should be made between team member and manager. The minimal recommended cadence for career development conversations is annnually, the most frequency recommended cadence is quarterly. 

### Process 

_Please note that while managers can facilitate career development conversations with team members and help guide growth plans by using our [internal resources](/handbook/people-group/learning-and-development/career-development/#internal-resources),  managers cannot complete the acknowledgement process for team members. The team member is responsible for the acknowledgement in BambooHR._

Starting FY'22 (exact date is TBD) team members will receive a notice via BambooHR to acknowledge that a career conversation has occurred. By signing the acknowledgement you are confirming that you have indeed had a career development conversation during the specified time period (Q2 or Q4 respectively).

*  This is not mandatory, however we highly encourage all team members to discuss their career goals with their manager.
*  There is not one right way to document a career development conversation. Some team members may use their 1:1 document to capture the conversation and actions, others may use the [tools provided above](/handbook/people-group/learning-and-development/career-development/#internal-resources) to help guide the conversation, or a team member may use their own personal template or process to have a career development conversation.
*  There is no timeline or deadline on this process, however, the goal is for all team members to acknowledge at least one career development conversation in FY'22. 
*  If you have not had a career conversation yet it is up to you to schedule a time to review and discuss with your manager.  
*  If you are new to GitLab the recommendation is that you start career conversations after your first 90 days.
*  Career conversations should not be confused with promotion conversations. Team members who do not want to increase their scope of work or be promoted, but are performing, should not feel pressured to move up or out. Career conversations can also focus on helping team members identify projects or other activities that keep the team member engaged and learning new skills. _Please note that development can also include lateral moves, or moving to another speciality within the same job family and job level._  

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/xUNupnJyTNY" frameborder="0" allowfullscreen="true"> </iframe>
</figure>


## E-Group Performance Enablement Review

Effective FY21, each member of the E-Group will have a performance conversation with their direct manager, the [CEO](/company/team/#sytses), at the beginning of the new fiscal year. These conversations will assess alignment to each of our [credit values](/handbook/values/#credit), performance and personal development.
 
The E-Group will follow a similar process to our GitLab team member process with the additional requirement of a [self review](https://docs.google.com/document/d/1pi-nv6NtMejosVOXlr1O4rZGxmGUzuN01OMnXn5dxKU/edit#).

**Performance Factors**

1. Developing in role, i.e., you are new to company / new to role or there is performance development required
1. Performing in role, i.e., you are meeting all requirements
1. Exceeding in role, i.e., you are knocking it out of the park

**Compensation Reviews**

When conducting compensation reviews for the E-Group, GitLab will review two items: 
1. Alignment to market rates 
1. Performance factors 


