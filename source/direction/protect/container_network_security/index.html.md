---
layout: secure_and_protect_direction
title: "Category Direction - Container Network Security"
description: "GitLab's container network security aims to allow any app to run on a cluster with any other app with confidence of defense against any unintended use or traffic"
canonical_path: "/direction/protect/container_network_security/"
---

- TOC
{:toc}

## Protect

| | |
| --- | --- |
| Stage | [Protect](/direction/protect/) |
| Maturity | [Minimal](/direction/maturity/) |
| Content Last Reviewed | `2022-02-02` |

### Introduction and how you can help
<!-- Introduce yourself and the category. Use this as an opportunity to point users to the right places for contributing and collaborating with you as the PM -->

<!--
<EXAMPLE>
Thanks for visiting this category direction page on Snippets in GitLab. This page belongs to the [Editor](/handbook/product/categories/#editor-group) group of the Create stage and is maintained by <PM NAME>([E-Mail](mailto:<EMAIL@gitlab.com>) [Twitter](https://twitter.com/<TWITTER>)).

This direction page is a work in progress, and everyone can contribute:

 - Please comment and contribute in the linked [issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=snippets) and [epics]((https://gitlab.com/groups/gitlab-org/-/epics?label_name[]=snippets) on this page. Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy and vision.
 - Please share feedback directly via email, Twitter, or on a video call. If you're a GitLab user and have direct knowledge of your need for snippets, we'd especially love to hear from you.
</EXAMPLE>
-->
Thanks for visiting this category direction page on Container Network Security in GitLab. This page belongs to the Container Security group of the Protect stage and is maintained by Sam White ([swhite@gitlab.com](mailto:<swhite@gitlab.com>)).

### Overview
Container Network Security involves filtering and securing the network traffic inside a containerized environment to enforce a least privilege access model and to block attacks at the network layer whenever possible.

#### Target Audience
<!--
List the personas (https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas#user-personas) involved in this category.

Look for differences in user's goals or uses that would affect their use of the product. Separate users and customers into different types based on those differences that make a difference.
-->
* [Sam (Security Analyst)](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#sam-security-analyst) and [Alex (Security Operations Engineer)](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#alex-security-operations-engineer) are our primary target personas for any organizations that have an established security team
* [Devon (DevOps Engineer)](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#devon-devops-engineer) could be a secondary persona for organizations without established security teams.  This needs to be researched more.

### Where we are Headed
<!--
Describe the future state for your category.
- What problems are we intending to solve?
- How will GitLab uniquely address them?
- What is the resulting benefits and value to users and their organizations?

Use narrative techniques to paint a picture of how the lives of your users will benefit from using this
category once your strategy is at least minimally realized. In order to challenge your level of ambition
(with the goal to make it sufficiently high), link to the current market leaders long-term vision and address how
we plan to displace them. -->

This category and all functionality related to it has been deprecated in the GitLab 14.8 release and is scheduled for removal in the GitLab 15.0 release.  Users who need a replacement for the functionality previously offered by this category are encouraged to evaluate the following open source projects as potential solutions that can be installed and managed outside of GitLab: [Cilium](https://github.com/cilium/cilium) and [FluentD](https://github.com/fluent/fluentd).

    As part of this change, the following specific capabilities within GitLab are now deprecated and are scheduled for removal in GitLab 15.0:

    - The **Security & Compliance > Threat Monitoring** page
    - The `Network Policy` type of security policy as found on the **Security & Compliance > Policies** page
    - The ability to manage integrations with the following technologies through GitLab: Cilium and FluentD
    - All APIs related to the above functionality