title: Weave
file_name: Weave
canonical_path: /customers/weave/
cover_image: /images/blogimages/weave_cover_image.jpg
cover_title: |
  GitLab delivers faster pipeline builds and improved code quality for Weave
cover_description: |
  Expanded use of GitLab drives process improvements that result in big efficiency gains at innovative Netherlands-based development consultancy.
twitter_image: /images/blogimages/weave_cover_image.jpg
twitter_text: Learn how GitLab delivers faster pipeline builds and improved code quality for Weave 
customer_logo: /images/case_study_logos/weave_logo.png
customer_logo_css_class: brand-logo-tall
customer_industry: Technology
customer_location: The Netherlands
customer_solution: GitLab Self Managed Premium
customer_employees: |
  25
customer_overview: |
  Weave provides innovative development teams able to build cloud-native apps for clients pursuing digital transformation. Dependable automated software builds are vital to success. 
customer_challenge: |
  Time consuming and error prone pipeline building caused Weave development teams to explore development tools alternatives.  The goal was to drive efficient cloud migrations and streamline state-of-the art Kubernetes practices while empowering development teams ready to exploit new tools’ features.
key_benefits: >-

  
    Supporting faster pipeline builds while boosting code quality 

  
    Freeing-up developer time for increased focus on application innovation

  
    Ensuring compliance with best practices in pipeline creation

  
    Better communication that drives co-innovation and collaboration with clients

  
    Kubernetes and Grafana integration


    Replaced Asana for project management
customer_stats:
  - stat: 23%
    label: Speed improvement in Google Cloud Platform (GCP) pipeline migration
  - stat:  30X
    label: Decrease in developer time to deploy pipelines
  - stat:  78X
    label: Increase in number of microservices deployed per week
customer_study_content:
  - title: the customer
    subtitle: Helping customers transform and innovate
    content: >-
  
       [Weave](https://weave.nl) is a developer of innovative software technologies focused on microservices and cloud-native development. The company leverages open source tools, including Go, Docker, and Kubernetes. The company’s objective is not merely to build and help clients transform their businesses, but to enable clients to carry their own development work forward as well. As it is a young company, Weave embraces what is new. Still, according to Peter-Jan Karens, CTO and co-founder, Weave’s experienced teams prioritize application deployment timelines. “Everybody wants to work with exciting new technologies, but clients want working applications delivered on time,” he said.

       Among Weave customers are Mijndomein, MobyOne, sustainable energy provider EnergyZero, and the Government of the Netherlands. Use of open source methods ensures that client’s applications are not dotted with unpredictable dependencies that bind them to proprietary software use. Weave works to help clients with legacy backlog to navigate a new software development landscape. The goal is to enable customers of all sizes to develop new applications on the cloud in much the same way as an Uber, Google, or any big tech company might. “We are trying to put this in the hands of our customers,” Karens said.


  - title: the challenge
    subtitle:  Lacking project management and Agile capabilities
    content: >-
    

        In expanding from the company’s original roots in PHP-oriented web development, Weave developers experimented with alternative ways of handling version control. But some processes were unstable, time consuming, and ‘messy.’ Speed of issue tracking, particularly, was found lacking, and the size of the hosting footprints was a drawback. Overall, Weave’s team concluded important portions of their existing tooling were not optimal for Agile development. Software alternatives for project management and software deployment were found at times to be little more than somewhat enhanced to-do lists, with rudimentary commenting. These had some use in project management, but were not truly a part of the actual development process. What was vitally needed, team members agreed, was closer coupling of project management and development. That meant integration tooling capable of handling issues, branches, merge requests, deployments, and pipelines. 


        Crucially missing were effective project management features like comprehensive milestones, as well as burn-down charts that showed required work remaining. These traits were deemed crucial to delivering software on time and providing process transparency to clients. In their quest, Weave managers also set forth the goal to find such capability in open source software. As they confronted factors limiting their ability to enable customers’ pressing digital transformation requirements, Weave staff began increasingly to use GitLab open source  software. As cloud application migrations became the common application pattern, the company began to deliberately move away from manual pipeline builds to automated GitLab deployments.


  - title: the solution
    subtitle:  Kubernetes integration, CI/CD builds, and improved collaboration
    content: >-


        The move to GitLab Premium coincided with expanded work with cloud and Kubernetes, and projects benefited from GitLab’s strong Kubernetes integration. As Weave implemented GitLab Premium, teams were able to dependably create repeatable standard processes and access a single source of truth covering the full software development lifecycle. Teams were enabled to quickly develop, test, and automate CI/CD pipeline builds to deploy to Kubernetes clusters. GitLab closely mirrored the company’s DevOps strategy, and provided a standard way to connect to the cloud and to deploy Kubernetes. “We really wanted not only to develop on Kubernetes, but also to deploy it through a CI/CD pipeline. And that was when we really started to see the benefits of GitLab,” said Peter-Jan Karens, CTO and Co-Founder. The migration of their GitLab instance to Google Kubernetes Engine (GKE) was “pretty smooth, frees up space and allows us to deploy more; really this migration aligns with our cloud journey and ensures we are at the forefront of innovation” Peter-Jan added. 

      
        After a brief time of experimentation, the software began to provide a capable template for repeatable deployment. Moreover, GitLab provided clients with transparency into activity, facilitating better communications all around. Weave leaders judge that use of GitLab today positively supports innovative development in small ways that in turn contribute to large and beneficial overall effect. Weave used GitLab to tighten compliance within the company. Yet, experience showed, it provided a natural environment within which developers could work. 

      

  - title: the results
    subtitle:  Making digital transformation possible
    content: >-

        In Weave’s experience, GitLab has dramatically improved the company’s Agile development program generally -- and several key processes specifically. For example, the speed of pipeline migration to the google cloud was boosted by 23%. This was accompanied by an estimated 30x decrease in developer time required to deploy pipelines. Meanwhile an increase in the number of microservices deployed per week is measured at 78x. “We have sprints with retrospectives … [and] … we want somehow to have these sprints supported in the software. Every development sprint is a milestone. We plan our issues, and then we can see how the sprint is progressing. This is super useful for us” added Peter-Jan Karens.


        Compliance helps ensure that high-quality projects are delivered on time, with guidelines for backend developers, front-end developers, and DevOps groups. A ready default pipeline repository ensures adherence to best practices. “With GitLab functionality and updates, we have insight into the process, which feels really nice,” according to Dorian de Koning, DevOps Leader. Finally, Weave is now better able to co-innovate with its clients via project collaborations based on GitLab. Team leaders have found that developers that work with other companies are familiar with GitLab style development, find it intuitive, and are able to be up and running on projects within just a few hours. In short, GitLab aligns with Weave values and development philosophy. 



   

        ## Learn more about GitLab solutions
    
  
        [Security with GitLab](/solutions/dev-sec-ops/)
    
  
        [CI with GitLab](/stages-devops-lifecycle/continuous-integration/)
    
  
        [Choose a plan that suits your needs](/pricing/)
customer_study_quotes:
  - blockquote: GitLab enables us to really share a lot of pipeline configurations, and Helm charts. It may not be exactly required to develop a project, but it will make a project more stable and maintainable. Also, you are always up-to-date. GitLab is a big factor in how fast we can implement pipelines, and in improving the quality of code
    attribution: Dorian de Koning
    attribution_title: DevOps Leader, Weave











